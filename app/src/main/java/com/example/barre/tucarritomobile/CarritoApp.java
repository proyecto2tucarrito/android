package com.example.barre.tucarritomobile;

import android.app.Application;

import com.example.barre.tucarritomobile.Carro.Carro;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.GoogleApiClient;

import java.io.IOException;

/**
 * Created by Barre on 06-Nov-17.
 */
public class CarritoApp extends Application {

    public GoogleApiClient mGoogleApiClient = null;
    public GoogleSignInAccount acct = null;
    public Carro carro = null;

    @Override
    public void onCreate() {
        super.onCreate();
        try {
            carro = new Carro();
            FacebookSdk.setApplicationId(Util.getProperty("AppId", getApplicationContext()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        //FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(getApplicationContext());
    }
}


