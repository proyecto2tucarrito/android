package com.example.barre.tucarritomobile.Carro;

import com.google.android.gms.analytics.ecommerce.Product;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by Barre on 19-Nov-17.
 */
public class Carro {
    private List<Item> list = new ArrayList<Item>();



    public List<Item> getList() {
        return list;
    }

    public void setList(List<Item> list) {
        this.list = list;
    }

    public int getCantidad(int idProducto){
        int ret = 0;

        ListIterator<Item> iter = this.list.listIterator();
        while(iter.hasNext()){
            Item item = (Item) iter.next();

            try {
                if(item.getProducto().getInt("id") == idProducto) {
                    ret = item.getCantidad();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return ret;
    }

    public int getCantidadTotal(){

        int ret = 0;

        ListIterator<Item> iter = this.list.listIterator();
        while(iter.hasNext()){
            Item item = (Item) iter.next();

            ret = ret + item.getCantidad();
        }

        return ret;
    }

    public String getPrecioTotal(){

        double ret = 0;
        DecimalFormat df2 = new DecimalFormat(".##");

        String retorno = "";

        ListIterator<Item> iter = this.list.listIterator();
        while(iter.hasNext()){
            Item item = (Item) iter.next();

            try {
                retorno = item.getProducto().getString("moneda");
                ret = ret + item.getProducto().getDouble("precioConDcto")*item.getCantidad();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return retorno + String.valueOf(df2.format(ret));
    }

    public Double getPrecioTotalDouble(){

        double ret = 0;

        ListIterator<Item> iter = this.list.listIterator();
        while(iter.hasNext()){
            Item item = (Item) iter.next();

            try {
                ret = ret + item.getProducto().getDouble("precioConDcto")*item.getCantidad();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return ret;
    }

    public void addItem(JSONObject producto){

        int index = 0;
        int indexLoop = 0;
        Item it = null;

        ListIterator<Item> iter = this.list.listIterator();
        while(iter.hasNext()){
            indexLoop = iter.nextIndex();
            Item item = (Item) iter.next();

            try {
                if(item.getProducto().getInt("id") == producto.getInt("id")) {
                    index = indexLoop;
                    it = item;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if(it != null)
        {
            it.setCantidad(it.getCantidad()+1);
            this.list.set(index, it);
        }
        else
        {
            Item itemJson = new Item(producto, 1);
            this.list.add(itemJson);
        }
    }

    public void setStockToProducto(int id, int stock){

        int index = 0;
        int indexLoop = 0;
        Item it = null;

        ListIterator<Item> iter = this.list.listIterator();
        while(iter.hasNext()){
            indexLoop = iter.nextIndex();
            Item item = (Item) iter.next();

            try {
                if(item.getProducto().getInt("id") == id) {
                    index = indexLoop;
                    it = item;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if(it != null)
        {
            try {
                JSONObject prod = it.getProducto();
                prod.put("stock", stock);

                it.setProducto(prod);
                this.list.set(index, it);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void removeItem(JSONObject producto){

        int index = 0;
        int indexLoop = 0;
        Item it = null;

        ListIterator<Item> iter = this.list.listIterator();
        while(iter.hasNext()){
            indexLoop = iter.nextIndex();
            Item item = (Item) iter.next();

            try {
                if(item.getProducto().getInt("id") == producto.getInt("id")) {
                    index = indexLoop;
                    it = item;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if(it != null)
        {
            if(it.getCantidad() == 1)
            {
                this.list.remove(it);
            }
            else
            {
                it.setCantidad(it.getCantidad()-1);
                this.list.set(index, it);
            }
        }
    }

    public void removeAllItem(JSONObject producto){

        Item it = null;

        ListIterator<Item> iter = this.list.listIterator();
        while(iter.hasNext()){
            Item item = (Item) iter.next();

            try {
                if(item.getProducto().getInt("id") == producto.getInt("id")) {
                    it = item;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if(it != null)
        {
            this.list.remove(it);
        }
    }


    /*public void cleanCarrito(){


    }*/

}
