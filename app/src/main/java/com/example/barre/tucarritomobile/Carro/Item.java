package com.example.barre.tucarritomobile.Carro;

import org.json.JSONObject;

/**
 * Created by Barre on 19-Nov-17.
 */
public class Item {
    private JSONObject producto;
    private int cantidad;

    public Item(JSONObject producto, int cantidad) {
        this.producto = producto;
        this.cantidad = cantidad;
    }

    public JSONObject getProducto() {
        return producto;
    }

    public void setProducto(JSONObject producto) {
        this.producto = producto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
}
