package com.example.barre.tucarritomobile.Cliente;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.barre.tucarritomobile.CarritoApp;
import com.example.barre.tucarritomobile.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

/**
 * Created by Barre on 19-Nov-17.
 */
public class AdaptadorCarro extends ArrayAdapter<String> {
    private String[] names;
    private String[] desc;
    private String[] precio;
    private String[] imageid;
    private String[] cantidades;
    private JSONObject[] jsonObjectArray;
    private CarritoApp carritoApp;
    private Activity context;

    public AdaptadorCarro(Activity context, String[] names, String[] desc, String[] precio, String[] imageid,
                          String[] cantidades, JSONObject[] jsonObjectArray) {
        super(context, R.layout.item_lista_carro, names);
        this.context = context;
        this.names = names;
        this.desc = desc;
        this.precio = precio;
        this.imageid = imageid;
        this.cantidades = cantidades;
        this.jsonObjectArray = jsonObjectArray;
        carritoApp = (CarritoApp) context.getApplicationContext();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.item_lista_carro, null, true);
        TextView textViewName = (TextView) listViewItem.findViewById(R.id.textViewName);
        TextView textViewDesc = (TextView) listViewItem.findViewById(R.id.textViewDesc);
        TextView textViewPrecio = (TextView) listViewItem.findViewById(R.id.textViewPrecio);
        final TextView textViewCantidad = (TextView) listViewItem.findViewById(R.id.textViewCantidad);
        ImageView image = (ImageView) listViewItem.findViewById(R.id.imageView);

        final ImageView imageMinus = (ImageView) listViewItem.findViewById(R.id.imageViewMinus);
        final ImageView imagePlus = (ImageView) listViewItem.findViewById(R.id.imageViewPlus);
        final ImageView imageRemove = (ImageView) listViewItem.findViewById(R.id.imageViewRemove);

        JSONObject jpersonObj = jsonObjectArray[position];
        imageMinus.setTag(jpersonObj.toString());
        imagePlus.setTag(jpersonObj.toString());
        imageRemove.setTag(jpersonObj.toString());

        textViewName.setText(names[position]);
        String textDesc = "";
        /*try {
            textDesc = new String(desc[position].getBytes("ISO-8859-1"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }*/
        textDesc = desc[position];

        textViewDesc.setText(textDesc);
        textViewPrecio.setText(precio[position]);
        textViewCantidad.setText(cantidades[position]);

        Picasso.with(getContext()).load(imageid[position]).into(image);


        imageMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String tag = (String) view.getTag();
                try {
                    //Remueve del carro
                    JSONObject newJsonObject = new JSONObject(tag);
                    carritoApp.carro.removeItem(newJsonObject);

                    /*if(textViewCantidad.getText().toString().equals("1"))
                    {
                        Intent intent = new Intent("cliente-carro");
                        LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(intent);
                    }
                    else
                    {
                        int cant = Integer.parseInt(textViewCantidad.getText().toString());
                        textViewCantidad.setText(String.valueOf(cant-1));
                    }*/

                    //Envio a los demas clientes
                    String id = String.valueOf(newJsonObject.getInt("id"));
                    String numberCant = String.valueOf(1);
                    Intent intentWebSocket = new Intent("cliente-sendMessage");
                    intentWebSocket.putExtra("message", id+","+numberCant+",+");
                    LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(intentWebSocket);

                    Intent intent = new Intent("cliente-carro");
                    LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(intent);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        imagePlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String tag = (String) view.getTag();
                try {
                    JSONObject newJsonObject = new JSONObject(tag);

                    if(newJsonObject.getInt("stock") != 0)
                    {
                        carritoApp.carro.addItem(newJsonObject);

                        //int cant = Integer.parseInt(textViewCantidad.getText().toString());
                        //textViewCantidad.setText(String.valueOf(cant+1));

                        //Envio a los demas clientes
                        String id = String.valueOf(newJsonObject.getInt("id"));
                        String numberCant = String.valueOf(1);
                        Intent intentWebSocket = new Intent("cliente-sendMessage");
                        intentWebSocket.putExtra("message", id + "," + numberCant + ",-");
                        LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(intentWebSocket);

                        Intent intent = new Intent("cliente-carro");
                        LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(intent);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        imageRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String tag = (String) view.getTag();
                try {
                    JSONObject newJsonObject = new JSONObject(tag);
                    carritoApp.carro.removeAllItem(newJsonObject);

                    //Envio a los demas clientes
                    String id = String.valueOf(newJsonObject.getInt("id"));
                    int cant = Integer.parseInt(textViewCantidad.getText().toString());
                    String numberCant = String.valueOf(cant);
                    Intent intentWebSocket = new Intent("cliente-sendMessage");
                    intentWebSocket.putExtra("message", id+","+numberCant+",+");
                    LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(intentWebSocket);

                    Intent intent = new Intent("cliente-carro");
                    LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(intent);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });


        return listViewItem;
    }
}