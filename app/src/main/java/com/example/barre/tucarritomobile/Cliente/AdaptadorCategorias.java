package com.example.barre.tucarritomobile.Cliente;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.barre.tucarritomobile.CarritoApp;
import com.example.barre.tucarritomobile.Carro.Item;
import com.example.barre.tucarritomobile.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.List;
import java.util.ListIterator;

/**
 * Adaptador para comidas usadas en la sección "Categorías"
 */
public class AdaptadorCategorias
        extends RecyclerView.Adapter<AdaptadorCategorias.ViewHolder> {

    private final JSONArray listProd;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView nombre;
        public TextView desc;
        public TextView precio;
        public ImageView imagen;
        public TextView marca;
        public TextView stock;
        public ImageView imageCarro;
        public ImageView imageMinus;
        public ImageView imagePlus;
        public TextView textViewCantidad;
        public CarritoApp carritoApp;

        public Context context;

        public ViewHolder(View v) {
            super(v);

            nombre = (TextView) v.findViewById(R.id.textViewName);
            desc = (TextView) v.findViewById(R.id.textViewDesc);
            precio = (TextView) v.findViewById(R.id.textViewPrecio);
            imagen = (ImageView) v.findViewById(R.id.imageView);
            marca = (TextView) v.findViewById(R.id.textViewMarca);
            stock = (TextView) v.findViewById(R.id.textViewStock);

            imageCarro = (ImageView) v.findViewById(R.id.imageViewCarro);
            imageMinus = (ImageView) v.findViewById(R.id.imageViewMinus);
            imagePlus = (ImageView) v.findViewById(R.id.imageViewPlus);
            textViewCantidad = (TextView) v.findViewById(R.id.textViewCantidad);

            carritoApp = (CarritoApp) v.getContext().getApplicationContext();
            context = v.getContext();

            imageCarro.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String tag = (String) view.getTag();
                    try {
                        JSONObject newJsonObject = new JSONObject(tag);

                        if(newJsonObject.getInt("stock") != 0)
                        {
                            //Agrego al carro
                            carritoApp.carro.addItem(newJsonObject);

                            //Cambio las imagenes
                            imageCarro.setVisibility(View.GONE);
                            imagePlus.setVisibility(View.VISIBLE);
                            imageMinus.setVisibility(View.VISIBLE);
                            textViewCantidad.setVisibility(View.VISIBLE);

                            //Envio a los demas clientes
                            String id = String.valueOf(newJsonObject.getInt("id"));
                            String numberCant = String.valueOf(1);
                            Intent intent = new Intent("cliente-sendMessage");
                            intent.putExtra("message", id+","+numberCant+",-");
                            LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(intent);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });

            imageMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String tag = (String) view.getTag();
                    try {
                        //Remuevo del carro
                        JSONObject newJsonObject = new JSONObject(tag);
                        carritoApp.carro.removeItem(newJsonObject);

                        //Cambia imagenes y cantidad
                        if(textViewCantidad.getText().toString().equals("1"))
                        {
                            imageCarro.setVisibility(View.VISIBLE);
                            imagePlus.setVisibility(View.GONE);
                            imageMinus.setVisibility(View.GONE);
                            textViewCantidad.setVisibility(View.GONE);
                        }
                        else
                        {
                            imageCarro.setVisibility(View.GONE);
                            imagePlus.setVisibility(View.VISIBLE);
                            imageMinus.setVisibility(View.VISIBLE);
                            textViewCantidad.setVisibility(View.VISIBLE);
                            int cant = Integer.parseInt(textViewCantidad.getText().toString());
                            textViewCantidad.setText(String.valueOf(cant-1));
                        }

                        //Envio a los demas clientes
                        String id = String.valueOf(newJsonObject.getInt("id"));
                        String numberCant = String.valueOf(1);
                        Intent intent = new Intent("cliente-sendMessage");
                        intent.putExtra("message", id+","+numberCant+",+");
                        LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(intent);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });

            imagePlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String tag = (String) view.getTag();
                    try {
                        JSONObject newJsonObject = new JSONObject(tag);

                        if(newJsonObject.getInt("stock") != 0)
                        {
                            //Agrego al carro
                            carritoApp.carro.addItem(newJsonObject);

                            //Cambio cantidad
                            int cant = Integer.parseInt(textViewCantidad.getText().toString());
                            textViewCantidad.setText(String.valueOf(cant + 1));

                            //Envio a los demas clientes
                            String id = String.valueOf(newJsonObject.getInt("id"));
                            String numberCant = String.valueOf(1);
                            Intent intent = new Intent("cliente-sendMessage");
                            intent.putExtra("message", id + "," + numberCant + ",-");
                            LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(intent);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });

        }
    }


    public AdaptadorCategorias(JSONArray listProd) {
        this.listProd = listProd;
    }

    @Override
    public int getItemCount() {
        return listProd.length();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_lista_categorias, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        JSONObject prod = null;
        JSONArray listaimg = null;
        JSONObject jsonMarca = null;
        try {
            prod = (JSONObject) listProd.get(i);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(prod != null) {

            DecimalFormat df2 = new DecimalFormat(".##");
            String textDesc = "";
            String textMarca = "";
            try {
                viewHolder.imageCarro.setTag(prod.toString());
                viewHolder.imageMinus.setTag(prod.toString());
                viewHolder.imagePlus.setTag(prod.toString());

                viewHolder.nombre.setText(prod.getString("nombre"));
                //textDesc = new String(prod.getString("descripcion").getBytes("ISO-8859-1"));
                jsonMarca = prod.getJSONObject("marca");
                //textMarca = new String(jsonMarca.getString("nombre").getBytes("ISO-8859-1"));

                textDesc = prod.getString("descripcion");
                textMarca = jsonMarca.getString("nombre");

                viewHolder.desc.setText(textDesc);
                viewHolder.precio.setText(prod.getString("moneda")+String.valueOf(df2.format(prod.getDouble("precioConDcto"))));

                viewHolder.marca.setText(textMarca);
                viewHolder.stock.setText("Hay "+String.valueOf(prod.getInt("stock"))+" en stock");
                listaimg = prod.getJSONArray("imagenes");
                JSONObject jsonImage = listaimg.getJSONObject(0);
                Picasso.with(viewHolder.itemView.getContext()).load(jsonImage.getString("imagen").replaceAll("\\'","'")).into(viewHolder.imagen);
            } catch (JSONException e) {
                e.printStackTrace();
            } /*catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }*/


            if(viewHolder.carritoApp.carro.getList().size() == 0)
            {
                viewHolder.imageCarro.setVisibility(View.VISIBLE);
                viewHolder.imagePlus.setVisibility(View.GONE);
                viewHolder.imageMinus.setVisibility(View.GONE);
                viewHolder.textViewCantidad.setVisibility(View.GONE);
            }
            else
            {
                viewHolder.imageCarro.setVisibility(View.VISIBLE);
                viewHolder.imagePlus.setVisibility(View.GONE);
                viewHolder.imageMinus.setVisibility(View.GONE);
                viewHolder.textViewCantidad.setVisibility(View.GONE);

                ListIterator<Item> iter = viewHolder.carritoApp.carro.getList().listIterator();
                while(iter.hasNext()){
                    Item item = (Item) iter.next();

                    try {
                        if(item.getProducto().getInt("id") == prod.getInt("id")) {
                            viewHolder.imageCarro.setVisibility(View.GONE);
                            viewHolder.imagePlus.setVisibility(View.VISIBLE);
                            viewHolder.imageMinus.setVisibility(View.VISIBLE);
                            viewHolder.textViewCantidad.setVisibility(View.VISIBLE);
                            viewHolder.textViewCantidad.setText(String.valueOf(item.getCantidad()));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }


            //Glide.with(viewHolder.itemView.getContext()).load(item.getIdDrawable()).centerCrop().into(viewHolder.imagen);
            //Picasso.with(viewHolder.itemView.getContext()).load(item.getIdDrawable()).into(viewHolder.imagen);


        }
    }


}