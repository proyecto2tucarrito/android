package com.example.barre.tucarritomobile.Cliente;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.barre.tucarritomobile.R;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Barre on 01-Dec-17.
 */
public class AdaptadorCompra extends ArrayAdapter<String> {
    private int[] ids;
    private String[] fechas;
    private String[] montos;
    private String[] estados;
    private String[] monedas;
    private Activity context;

    public AdaptadorCompra(Activity context, int[] ids, String[] fechas, String[] montos, String[] estados, String[] monedas) {
        super(context, R.layout.item_lista_compra, estados);
        this.context = context;
        this.ids = ids;
        this.fechas = fechas;
        this.montos = montos;
        this.estados = estados;
        this.monedas = monedas;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.item_lista_compra, null, true);

        TextView textViewId = (TextView) listViewItem.findViewById(R.id.textViewId);
        TextView textViewFecha = (TextView) listViewItem.findViewById(R.id.textViewFecha);
        TextView textViewMonto = (TextView) listViewItem.findViewById(R.id.textViewMonto);
        TextView textViewEstado = (TextView) listViewItem.findViewById(R.id.textViewEstado);

        textViewId.setText(String.valueOf(ids[position]));
        textViewFecha.setText(fechas[position]);
        textViewMonto.setText(monedas[position]+montos[position]);
        if(estados[position].equals("RECIBIDO"))
        {
            textViewEstado.setTextColor(ContextCompat.getColor(context, R.color.color_red));
        }
        textViewEstado.setText(estados[position]);

        listViewItem.setTag(String.valueOf(ids[position]));

        listViewItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String tag = (String) view.getTag();

                Intent intent = new Intent("cliente-openCompraDetail");
                intent.putExtra("idCompra", tag);
                LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(intent);

            }
        });

        return listViewItem;
    }
}