package com.example.barre.tucarritomobile.Cliente;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.barre.tucarritomobile.R;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;

/**
 * Created by Barre on 01-Dec-17.
 */
public class AdaptadorCompraDetail extends ArrayAdapter<String> {
    private int[] ids;
    private String[] cantidades;
    private String[] precios;
    private String[] totales;
    private String[] monedas;
    private String[] nombres;
    private String[] descripciones;
    private String[] imagenes;
    private Activity context;

    public AdaptadorCompraDetail(Activity context, int[] ids, String[] cantidades, String[] precios, String[] totales, String[] monedas,
                                 String[] nombres, String[] descripciones, String[] imagenes) {
        super(context, R.layout.item_lista_compra_detail, nombres);
        this.context = context;
        this.ids = ids;
        this.cantidades = cantidades;
        this.precios = precios;
        this.totales = totales;
        this.monedas = monedas;
        this.nombres = nombres;
        this.descripciones = descripciones;
        this.imagenes = imagenes;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.item_lista_compra_detail, null, true);

        ImageView image = (ImageView) listViewItem.findViewById(R.id.imageView);
        TextView textViewName = (TextView) listViewItem.findViewById(R.id.textViewName);
        TextView textViewDesc = (TextView) listViewItem.findViewById(R.id.textViewDesc);

        TextView textViewCantidad = (TextView) listViewItem.findViewById(R.id.textViewCantidad);
        TextView textViewPrecio = (TextView) listViewItem.findViewById(R.id.textViewPrecio);
        TextView textViewTotal = (TextView) listViewItem.findViewById(R.id.textViewTotal);

        Picasso.with(getContext()).load(imagenes[position]).into(image);
        textViewName.setText(nombres[position]);
        String textDesc = "";
        /*try {
            textDesc = new String(descripciones[position].getBytes("ISO-8859-1"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }*/
        textDesc = descripciones[position];

        textViewDesc.setText(textDesc);

        textViewCantidad.setText(cantidades[position]);
        String precio = monedas[position]+precios[position];
        textViewPrecio.setText(precio);
        String total = monedas[position]+totales[position];
        textViewTotal.setText(total);




        return listViewItem;
    }
}
