package com.example.barre.tucarritomobile.Cliente;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.barre.tucarritomobile.CarritoApp;
import com.example.barre.tucarritomobile.Carro.Item;
import com.example.barre.tucarritomobile.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ListIterator;


/**
 * Created by Barre on 15-Nov-17.
 */
public class AdaptadorInicioProducto extends ArrayAdapter<String> {
    private int[] ids;
    private String[] names;
    private String[] desc;
    private String[] precio;
    private String[] imageid;
    private String[] stocks;
    private String[] marcas;
    private Activity context;
    private CarritoApp carritoApp;
    private JSONArray lista;

    public AdaptadorInicioProducto(Activity context, int[] ids, String[] names, String[] desc, String[] precio, String[] imageid,
                                   String[] stocks, String[] marcas, JSONArray lista) {
        super(context, R.layout.item_lista_inicio_producto, names);
        this.context = context;
        this.ids = ids;
        this.names = names;
        this.desc = desc;
        this.precio = precio;
        this.imageid = imageid;
        this.stocks = stocks;
        this.marcas = marcas;
        this.lista = lista;
        carritoApp = (CarritoApp) context.getApplicationContext();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.item_lista_inicio_producto, null, true);
        TextView textViewName = (TextView) listViewItem.findViewById(R.id.textViewName);
        TextView textViewDesc = (TextView) listViewItem.findViewById(R.id.textViewDesc);
        TextView textViewPrecio = (TextView) listViewItem.findViewById(R.id.textViewPrecio);
        TextView textViewStock = (TextView) listViewItem.findViewById(R.id.textViewStock);
        TextView textViewMarca = (TextView) listViewItem.findViewById(R.id.textViewMarca);
        ImageView image = (ImageView) listViewItem.findViewById(R.id.imageView);

        final ImageView imageCarro = (ImageView) listViewItem.findViewById(R.id.imageViewCarro);
        final ImageView imageMinus = (ImageView) listViewItem.findViewById(R.id.imageViewMinus);
        final ImageView imagePlus = (ImageView) listViewItem.findViewById(R.id.imageViewPlus);
        final TextView textViewCantidad = (TextView) listViewItem.findViewById(R.id.textViewCantidad);


        try {
            JSONObject jpersonObj = lista.getJSONObject(position);
            imageCarro.setTag(jpersonObj.toString());
            imageMinus.setTag(jpersonObj.toString());
            imagePlus.setTag(jpersonObj.toString());
            listViewItem.setTag(String.valueOf(ids[position]));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        textViewName.setText(names[position]);
        String textDesc = "";
        String textMarca = "";
        /*try {
            textDesc = new String(desc[position].getBytes("ISO-8859-1"));
            textMarca = new String(marcas[position].getBytes("ISO-8859-1"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }*/
        textDesc = desc[position];
        textMarca = marcas[position];

        textViewDesc.setText(textDesc);
        textViewPrecio.setText(precio[position]);
        textViewMarca.setText(textMarca);
        textViewStock.setText(stocks[position]);

        Picasso.with(getContext()).load(imageid[position]).into(image);

        if(carritoApp.carro.getList().size() == 0)
        {
            imageCarro.setVisibility(View.VISIBLE);
            imagePlus.setVisibility(View.GONE);
            imageMinus.setVisibility(View.GONE);
            textViewCantidad.setVisibility(View.GONE);
        }
        else
        {
            imageCarro.setVisibility(View.VISIBLE);
            imagePlus.setVisibility(View.GONE);
            imageMinus.setVisibility(View.GONE);
            textViewCantidad.setVisibility(View.GONE);

            ListIterator<Item> iter = carritoApp.carro.getList().listIterator();
            while(iter.hasNext()){
                Item item = (Item) iter.next();

                try {
                    if(item.getProducto().getInt("id") == ids[position]) {
                        imageCarro.setVisibility(View.GONE);
                        imagePlus.setVisibility(View.VISIBLE);
                        imageMinus.setVisibility(View.VISIBLE);
                        textViewCantidad.setVisibility(View.VISIBLE);
                        textViewCantidad.setText(String.valueOf(item.getCantidad()));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        imageCarro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String tag = (String) view.getTag();
                try {
                    JSONObject newJsonObject = new JSONObject(tag);

                    if(newJsonObject.getInt("stock") != 0)
                    {
                        //Agrego al carro
                        carritoApp.carro.addItem(newJsonObject);

                        //Cambio las imagenes
                        imageCarro.setVisibility(View.GONE);
                        imagePlus.setVisibility(View.VISIBLE);
                        imageMinus.setVisibility(View.VISIBLE);
                        textViewCantidad.setVisibility(View.VISIBLE);

                        //Envio a los demas clientes
                        String id = String.valueOf(newJsonObject.getInt("id"));
                        String numberCant = String.valueOf(1);
                        Intent intent = new Intent("cliente-sendMessage");
                        intent.putExtra("message", id+","+numberCant+",-");
                        LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(intent);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        imageMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String tag = (String) view.getTag();
                try {
                    //Remuevo del carro
                    JSONObject newJsonObject = new JSONObject(tag);
                    carritoApp.carro.removeItem(newJsonObject);

                    //Cambia imagenes y cantidad
                    if(textViewCantidad.getText().toString().equals("1"))
                    {
                        imageCarro.setVisibility(View.VISIBLE);
                        imagePlus.setVisibility(View.GONE);
                        imageMinus.setVisibility(View.GONE);
                        textViewCantidad.setVisibility(View.GONE);
                    }
                    else
                    {
                        imageCarro.setVisibility(View.GONE);
                        imagePlus.setVisibility(View.VISIBLE);
                        imageMinus.setVisibility(View.VISIBLE);
                        textViewCantidad.setVisibility(View.VISIBLE);
                        int cant = Integer.parseInt(textViewCantidad.getText().toString());
                        textViewCantidad.setText(String.valueOf(cant-1));
                    }

                    //Envio a los demas clientes
                    String id = String.valueOf(newJsonObject.getInt("id"));
                    String numberCant = String.valueOf(1);
                    Intent intent = new Intent("cliente-sendMessage");
                    intent.putExtra("message", id+","+numberCant+",+");
                    LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(intent);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        imagePlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String tag = (String) view.getTag();
                try {
                    JSONObject newJsonObject = new JSONObject(tag);

                    if(newJsonObject.getInt("stock") != 0)
                    {
                        //Agrego al carro
                        carritoApp.carro.addItem(newJsonObject);

                        //Cambio cantidad
                        int cant = Integer.parseInt(textViewCantidad.getText().toString());
                        textViewCantidad.setText(String.valueOf(cant + 1));

                        //Envio a los demas clientes
                        String id = String.valueOf(newJsonObject.getInt("id"));
                        String numberCant = String.valueOf(1);
                        Intent intent = new Intent("cliente-sendMessage");
                        intent.putExtra("message", id + "," + numberCant + ",-");
                        LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(intent);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        listViewItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String tag = (String) view.getTag();

                Intent intent = new Intent("cliente-openProductoDetail");
                intent.putExtra("idProducto", tag);
                LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(intent);

            }
        });

        return  listViewItem;
    }


}
