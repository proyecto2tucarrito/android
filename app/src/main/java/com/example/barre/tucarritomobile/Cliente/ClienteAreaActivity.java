package com.example.barre.tucarritomobile.Cliente;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.barre.tucarritomobile.CarritoApp;
import com.example.barre.tucarritomobile.Carro.Item;
import com.example.barre.tucarritomobile.Firebase.LoadTokenService;
import com.example.barre.tucarritomobile.R;
import com.example.barre.tucarritomobile.Util;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;

/**
 * Created by Barre on 06-Nov-17.
 */
public class ClienteAreaActivity extends AppCompatActivity {

    private Menu menu;
    private DrawerLayout drawerLayout;
    private NavigationView navigationViewCre;
    private int id;
    private String mail;
    private String nombre;
    private String imagen;
    private String direccion;
    private OkHttpClient client;
    private WebSocket ws;
    private CarritoApp carritoApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_principal);


        client = new OkHttpClient();
        NavigationView navView = (NavigationView) findViewById(R.id.nav_view);
        View header = navView.getHeaderView(0);
        ImageView image = (ImageView) header.findViewById(R.id.imageView3);

        try {
            Picasso.with(this).load(Util.getProperty("imagen_logo", this)).resize(1700, 350).into(image);
            //LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(200,LinearLayout.LayoutParams.WRAP_CONTENT);
            //image.setLayoutParams(parms);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Bundle bundle = getIntent().getExtras();
        id = bundle.getInt("id");
        mail = bundle.getString("mail");
        nombre = bundle.getString("nombre");
        imagen = bundle.getString("imagen");
        direccion = bundle.getString("direccion");

        if(direccion != null)
        {
            if(!direccion.equals(""))
            {
                direccion = direccion.replaceAll("\\'","'");
            }
        }
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = pref.edit();

        editor.putInt("id", id);
        editor.putString("mail", mail);
        editor.putString("nombre", nombre);
        editor.putString("imagen", imagen);
        editor.putString("direccion", direccion);
        editor.commit();

        Intent tokenIntent = new Intent(ClienteAreaActivity.this, LoadTokenService.class);
        startService(tokenIntent);

        agregarToolbar();

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationViewCre = (NavigationView) findViewById(R.id.nav_view);


        if (navigationViewCre != null) {
            prepararDrawer(navigationViewCre);
            // Seleccionar item por defecto
            seleccionarItem(navigationViewCre.getMenu().getItem(0));
        }

        start();
    }

    private void agregarToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            // Poner ícono del drawer toggle
            ab.setHomeAsUpIndicator(R.drawable.drawer_toggle);
            ab.setDisplayHomeAsUpEnabled(true);
        }

    }

    private void prepararDrawer(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        seleccionarItem(menuItem);
                        drawerLayout.closeDrawers();
                        return true;
                    }
                });

    }

    private void seleccionarItem(MenuItem itemDrawer) {
        Fragment fragmentoGenerico = null;
        FragmentManager fragmentManager = getSupportFragmentManager();

        switch (itemDrawer.getItemId()) {
            case R.id.item_inicio:
                fragmentoGenerico = new FragmentoInicioProducto();
                //fragmentoGenerico = new FragmentoInicio();
                break;
            case R.id.item_cuenta:
                fragmentoGenerico = new FragmentoCuenta();
                break;
            case R.id.item_categorias:
                fragmentoGenerico = new FragmentoCategorias();
                break;
            case R.id.item_carro:
                fragmentoGenerico = new FragmentoCarro();
                break;
            case R.id.item_compra:
                fragmentoGenerico = new FragmentoCompra();
                break;
            case R.id.item_logout:
                fragmentoGenerico = new FragmentoLogout();
                break;
        }
        if (fragmentoGenerico != null) {
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.contenedor_principal, fragmentoGenerico)
                    .commit();
        }

        // Setear título actual
        setTitle(itemDrawer.getTitle());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_actividad_principal, menu);
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void goToCarrito(MenuItem item){
        navigationViewCre.getMenu().getItem(3).setChecked(true);
        seleccionarItem(navigationViewCre.getMenu().getItem(3));
        drawerLayout.closeDrawers();
    }

    private void destroyCarrito(){
        carritoApp = (CarritoApp) getApplicationContext();
        List<Item> list = carritoApp.carro.getList();

        int idProducto = 0;
        int numberCant = 0;
        ListIterator<Item> iter = list.listIterator();
        while(iter.hasNext()){
            Item item = (Item) iter.next();

            try {
                idProducto = item.getProducto().getInt("id");
                numberCant = item.getCantidad();
                ws.send(idProducto+","+numberCant+",+");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        List<Item> listToSet = new ArrayList<Item>();
        carritoApp.carro.setList(listToSet);
    }

    @Override
    public void onResume(){
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageDireccion,
                new IntentFilter("cliente-direccion"));
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageCarro,
                new IntentFilter("cliente-carro"));
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageWebview,
                new IntentFilter("cliente-webview"));
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageCloseWebview,
                new IntentFilter("cliente-closeWebview"));
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageSendMessage,
                new IntentFilter("cliente-sendMessage"));
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageCompraDetail,
                new IntentFilter("cliente-openCompraDetail"));
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageProductoDetail,
                new IntentFilter("cliente-openProductoDetail"));
    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageDireccion);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageCarro);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageWebview);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageCloseWebview);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageSendMessage);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageCompraDetail);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageProductoDetail);
        super.onPause();
    }

    @Override
    public void onDestroy() {

        //llamar a una funcion para vaciar el carrito
        this.destroyCarrito();

        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageDireccion);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageCarro);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageWebview);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageCloseWebview);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageSendMessage);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageCompraDetail);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageProductoDetail);


        super.onDestroy();
    }

    private BroadcastReceiver mMessageDireccion = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            navigationViewCre.getMenu().getItem(1).setChecked(true);
            seleccionarItem(navigationViewCre.getMenu().getItem(1));
            drawerLayout.closeDrawers();

        }
    };

    private BroadcastReceiver mMessageCarro = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            navigationViewCre.getMenu().getItem(3).setChecked(true);
            seleccionarItem(navigationViewCre.getMenu().getItem(3));
            drawerLayout.closeDrawers();
        }
    };

    private BroadcastReceiver mMessageWebview = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String url = intent.getStringExtra("url");
            Fragment fragmentoGenerico = null;
            FragmentManager fragmentManager = getSupportFragmentManager();

            fragmentoGenerico = FragmentoWebView.nuevaInstancia(url);

            if (fragmentoGenerico != null) {
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.contenedor_principal, fragmentoGenerico)
                        .commit();
            }

        }
    };

    private BroadcastReceiver mMessageCompraDetail = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String idCompra = intent.getStringExtra("idCompra");
            Fragment fragmentoGenerico = null;
            FragmentManager fragmentManager = getSupportFragmentManager();

            fragmentoGenerico = FragmentoCompraDetail.nuevaInstancia(Integer.parseInt(idCompra));

            if (fragmentoGenerico != null) {
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.contenedor_principal, fragmentoGenerico)
                        .commit();
            }

        }
    };

    private BroadcastReceiver mMessageProductoDetail = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String idProducto = intent.getStringExtra("idProducto");
            Fragment fragmentoGenerico = null;
            FragmentManager fragmentManager = getSupportFragmentManager();

            fragmentoGenerico = FragmentoProductoDetail.nuevaInstancia(Integer.parseInt(idProducto));

            if (fragmentoGenerico != null) {
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.contenedor_principal, fragmentoGenerico)
                        .commit();
            }

        }
    };

    private BroadcastReceiver mMessageCloseWebview = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String desc = intent.getStringExtra("descripcion");
            if(desc.equals("true")){
                carritoApp = (CarritoApp) context.getApplicationContext();
                List<Item> listToSet = new ArrayList<Item>();
                carritoApp.carro.setList(listToSet);
                navigationViewCre.getMenu().getItem(0).setChecked(true);
                seleccionarItem(navigationViewCre.getMenu().getItem(0));
                drawerLayout.closeDrawers();
                Toast.makeText(getApplicationContext(), R.string.action_compra_realizada, Toast.LENGTH_SHORT).show();
            }

        }
    };

    private BroadcastReceiver mMessageSendMessage = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String message = intent.getStringExtra("message");
            ws.send(message);
        }
    };

    private void start() {
        String locationUrl = null;
        String IP = null;
        try {
            locationUrl = Util.getProperty("LocationUrl", this);
            IP = Util.getProperty("IP", this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        byte[] locationUrlByte = locationUrl.getBytes();
        locationUrl = Base64.encodeToString(locationUrlByte, Base64.DEFAULT);
        locationUrl = locationUrl.replace("\n", "");
        Request request = new Request.Builder().url("ws://"+IP+":8080/tuCarrito-web/productoSocket/"+locationUrl).build();
        EchoWebSocketListener listener = new EchoWebSocketListener();
        ws = client.newWebSocket(request, listener);

        //ws.send("Hello, it's SSaurel !");

        client.dispatcher().executorService().shutdown();
    }


    private final class EchoWebSocketListener extends WebSocketListener {
        private static final int NORMAL_CLOSURE_STATUS = 1000;

        @Override
        public void onOpen(WebSocket webSocket, Response response) {

        }

        @Override
        public void onMessage(WebSocket webSocket, String text) {
            Intent intent = new Intent("websocket-message");
            intent.putExtra("message", text);
            carritoApp = (CarritoApp) getApplicationContext();
            String[] parts = text.split(",");
            carritoApp.carro.setStockToProducto(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]));
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
        }

        @Override
        public void onClosing(WebSocket webSocket, int code, String reason) {
            webSocket.close(NORMAL_CLOSURE_STATUS, null);


        }

        @Override
        public void onFailure(WebSocket webSocket, Throwable t, Response response) {

        }
    }
}
