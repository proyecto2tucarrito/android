package com.example.barre.tucarritomobile.Cliente;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.barre.tucarritomobile.CarritoApp;
import com.example.barre.tucarritomobile.Carro.Item;
import com.example.barre.tucarritomobile.CompraRequest.InsertarCompraPendienteRequest;
import com.example.barre.tucarritomobile.CompraRequest.InsertarCompraRequest;
import com.example.barre.tucarritomobile.ProductoRequest.ObtenerProductosRequest;
import com.example.barre.tucarritomobile.R;
import com.example.barre.tucarritomobile.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by Barre on 19-Nov-17.
 */
public class FragmentoCarro extends Fragment {

    private ListView listView;
    private String names[];
    private String desc[];
    private String precio[];
    private String imageid[];
    private String cantidades[];
    private JSONObject jsonObjectArray[];
    private ProgressDialog loading;
    private CarritoApp carritoApp;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        carritoApp = (CarritoApp) getContext().getApplicationContext();

        View v = inflater.inflate(R.layout.fragmento_carro, container, false);

        listView = (ListView) v.findViewById(R.id.listViewCli);

        TextView textViewTotal = (TextView) v.findViewById(R.id.textViewTotal);

        String precioTotal = carritoApp.carro.getPrecioTotal();
        textViewTotal.setText(precioTotal);

        Button buttonComprar = (Button) v.findViewById(R.id.buttonComprar);



        buttonComprar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getContext());
                String direccion = pref.getString("direccion", null);

                if(direccion != null){
                    if(!direccion.equals("")){
                        new InsertarCompraRequest(getContext()).execute();
                    }
                    else{
                        Toast.makeText(getContext().getApplicationContext(), R.string.texto_no_direccion, Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(getContext().getApplicationContext(), R.string.texto_no_direccion, Toast.LENGTH_SHORT).show();
                }

            }
        });

        loading = new ProgressDialog(v.getContext());
        loading.setMessage("Cargando...");
        loading.setCancelable(false);
        loading.show();

        showInformation(carritoApp.carro.getList());

        return v;
    }

    @Override
    public void onResume(){
        super.onResume();
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).registerReceiver(mMessageGetListaProductos,
                new IntentFilter("cliente-productos"));
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).registerReceiver(mMessageInsertCompra,
                new IntentFilter("compra-insertar"));
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).registerReceiver(mMessageRedirectCompra,
                new IntentFilter("compra-redirect"));

    }


    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).unregisterReceiver(mMessageGetListaProductos);
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).unregisterReceiver(mMessageInsertCompra);
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).unregisterReceiver(mMessageRedirectCompra);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).unregisterReceiver(mMessageGetListaProductos);
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).unregisterReceiver(mMessageInsertCompra);
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).unregisterReceiver(mMessageRedirectCompra);
        super.onDestroy();
    }

    private BroadcastReceiver mMessageGetListaProductos = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String listaProductos = intent.getStringExtra("listaProductos");
            Log.v("LISTASOL", listaProductos);
            if(listaProductos.equals("[]")){
                loading.dismiss();
            }else{
                //showInformation(listaProductos);
            }
        }
    };

    private BroadcastReceiver mMessageInsertCompra = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String compra = intent.getStringExtra("compra");
            insertCompraPendiente(compra);
        }
    };

    private BroadcastReceiver mMessageRedirectCompra = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String param = intent.getStringExtra("param");
            sendRedirect(param);
        }
    };

    private void sendRedirect(String param){
        Intent intent = new Intent("cliente-webview");
        try {
            String IP_PAS = Util.getProperty("IP_PASARELA", getContext());

            JSONObject jsonObject = new JSONObject(param);
            String paramId = jsonObject.getString("param");
            //String url = "https://"+ IP_PAS +":8443/pasarela-web/pago.xhtml?param="+paramId;
            String url = "http://"+ IP_PAS +":8080/pasarela-web/pago.xhtml?param="+paramId;

            intent.putExtra("url", url);
            LocalBroadcastManager.getInstance(getContext().getApplicationContext()).sendBroadcast(intent);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void insertCompraPendiente(String compra){
        try {
            JSONObject jsonObject = new JSONObject(compra);
            int numOrden = jsonObject.getInt("id");
            String monto = jsonObject.getString("monto_total");

            new InsertarCompraPendienteRequest(getContext()).execute(String.valueOf(numOrden), monto);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    public void showInformation(List<Item> listaItem){
        JSONArray listaimg = null;
        DecimalFormat df2 = new DecimalFormat(".##");
        try {
            names = new String [listaItem.size()];
            desc = new String [listaItem.size()];
            precio = new String [listaItem.size()];
            imageid = new String [listaItem.size()];
            cantidades = new String [listaItem.size()];
            jsonObjectArray = new JSONObject [listaItem.size()];
            for (int i=0; i < listaItem.size(); i++) {
                Item item = listaItem.get(i);
                JSONObject jpersonObj = item.getProducto();
                names[i] = jpersonObj.getString("nombre");
                desc[i] = jpersonObj.getString("descripcion");
                /*if(df2.format(jpersonObj.getDouble("descuento")).equals(0.00))
                {

                }*/
                precio[i] = jpersonObj.getString("moneda")+String.valueOf(df2.format(jpersonObj.getDouble("precioConDcto")));

                listaimg = jpersonObj.getJSONArray("imagenes");
                JSONObject jsonImage = listaimg.getJSONObject(0);
                imageid[i] = jsonImage.getString("imagen").replaceAll("\\'","'");
                cantidades[i] = String.valueOf(item.getCantidad());
                jsonObjectArray[i] = jpersonObj;
            }
            AdaptadorCarro adaptadorCarro = new AdaptadorCarro(getActivity(), names, desc, precio, imageid, cantidades, jsonObjectArray);
            if((loading != null) && loading.isShowing()){
                loading.dismiss();
            }
            listView.setAdapter(adaptadorCarro);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }



}

