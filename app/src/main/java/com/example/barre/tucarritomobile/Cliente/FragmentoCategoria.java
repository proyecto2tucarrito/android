package com.example.barre.tucarritomobile.Cliente;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.barre.tucarritomobile.ProductoRequest.ObtenerProductosRequest;
import com.example.barre.tucarritomobile.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * Fragmento que representa el contenido de cada pestaña dentro de la sección "Categorías"
 */
public class FragmentoCategoria extends Fragment {

    private static final String INDICE_SECCION
            = "com.restaurantericoparico.FragmentoCategoriasTab.extra.INDICE_SECCION";

    private static final String LIST_PROD
            = "LIST_PROD";

    private static final String LIST_CATEG
            = "LIST_CATEG";

    private RecyclerView reciclador;
    private GridLayoutManager layoutManager;
    private AdaptadorCategorias adaptador;

    public static FragmentoCategoria nuevaInstancia(int indiceSeccion, JSONArray listProd, JSONArray listCateg) {
        FragmentoCategoria fragment = new FragmentoCategoria();

        //new ObtenerProductosRequest(fragment.getContext()).execute();


        Bundle args = new Bundle();
        args.putInt(INDICE_SECCION, indiceSeccion);
        if(listProd != null){
            args.putString(LIST_PROD, listProd.toString());
        }

        if(listCateg != null){
            args.putString(LIST_CATEG, listCateg.toString());
        }

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmento_grupo_items, container, false);

        reciclador = (RecyclerView) view.findViewById(R.id.reciclador);
        layoutManager = new GridLayoutManager(getActivity(), 1);
        reciclador.setLayoutManager(layoutManager);


        constructAdaptadorCateg();


        /*int indiceSeccion = getArguments().getInt(INDICE_SECCION);
        switch (indiceSeccion) {
            case 0:
                adaptador = new AdaptadorCategorias(Comida.PLATILLOS);
                break;
            case 1:
                adaptador = new AdaptadorCategorias(Comida.BEBIDAS);
                break;
            case 2:
                adaptador = new AdaptadorCategorias(Comida.POSTRES);
                break;
        }*/

        reciclador.setAdapter(adaptador);

        return view;
    }

    /*@Override
    public void onResume(){
        super.onResume();
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).registerReceiver(mMessageGetProductos,
                new IntentFilter("cliente-productos"));
    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).unregisterReceiver(mMessageGetProductos);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).unregisterReceiver(mMessageGetProductos);
        super.onDestroy();
    }

    private BroadcastReceiver mMessageGetProductos = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String listaProductos = intent.getStringExtra("listaProductos");
            constructAdaptadorCateg(listaProductos);
        }
    };

    */
    private void constructAdaptadorCateg(){
        int indiceSeccion = getArguments().getInt(INDICE_SECCION);
        String sListProd = getArguments().getString(LIST_PROD);
        String sListCateg = getArguments().getString(LIST_CATEG);

        JSONArray listProd;
        JSONArray listCateg;
        try {
            listProd = new JSONArray(sListProd);
            listCateg = new JSONArray(sListCateg);
            for (int i=0; i < listCateg.length(); i++) {
                adaptador = new AdaptadorCategorias(listProd);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        reciclador.setAdapter(adaptador);
    }

}
