package com.example.barre.tucarritomobile.Cliente;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;


import com.example.barre.tucarritomobile.ProductoRequest.ObtenerCategoriasRequest;
import com.example.barre.tucarritomobile.ProductoRequest.ObtenerProductosRequest;
import com.example.barre.tucarritomobile.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * Fragmento que contiene otros fragmentos anidados para representar las categorías
 * de comidas
 */
public class FragmentoCategorias extends Fragment {
    private AppBarLayout appBarLayout;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ProgressDialog loading;

    private JSONArray listProd = null;

    public FragmentoCategorias() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmento_paginado, container, false);

        if (savedInstanceState == null) {
            loading = new ProgressDialog(view.getContext());
            loading.setMessage("Cargando...");
            loading.setCancelable(false);
            loading.show();

            insertarTabs(container);

            // Setear adaptador al viewpager.
            viewPager = (ViewPager) view.findViewById(R.id.pager);

            new ObtenerProductosRequest(getContext()).execute();
            //poblarViewPager(viewPager);

            tabLayout.setupWithViewPager(viewPager);
        }

        return view;
    }

    private void insertarTabs(ViewGroup container) {
        View padre = (View) container.getParent();
        appBarLayout = (AppBarLayout) padre.findViewById(R.id.appbar);

        tabLayout = new TabLayout(getActivity());
        tabLayout.setTabTextColors(Color.parseColor("#FFFFFF"), Color.parseColor("#FFFFFF"));
        appBarLayout.addView(tabLayout);
    }

   /* private void poblarViewPager(ViewPager viewPager) {
        AdaptadorSecciones adapter = new AdaptadorSecciones(getFragmentManager());
        adapter.addFragment(FragmentoCategoria.nuevaInstancia(0), getString(R.string.titulo_tab_platillos));
        adapter.addFragment(FragmentoCategoria.nuevaInstancia(1), getString(R.string.titulo_tab_bebidas));
        adapter.addFragment(FragmentoCategoria.nuevaInstancia(2), getString(R.string.titulo_tab_postres));
        viewPager.setAdapter(adapter);
    }*/

    private void poblarViewPager(String listaCategorias) {
        JSONArray lista = null;
        AdaptadorSecciones adapter = new AdaptadorSecciones(getFragmentManager());
        try {
            lista = new JSONArray(listaCategorias);
            for (int i=0; i < lista.length(); i++) {
                JSONObject jpersonObj = lista.getJSONObject(i);
                JSONArray listToSend = getListProdFromCateg(jpersonObj.getInt("id"));
                adapter.addFragment(FragmentoCategoria.nuevaInstancia(i, listToSend, lista), jpersonObj.getString("nombre"));
            }
            loading.dismiss();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        viewPager.setAdapter(adapter);
    }

    private JSONArray getListProdFromCateg(int idCateg){
        JSONArray ret = null;
        if(listProd != null){
            try {
                ret = new JSONArray();
                for (int i=0; i < listProd.length(); i++) {
                    JSONObject jpersonObj = listProd.getJSONObject(i);
                    if(jpersonObj.getJSONObject("categoria").getInt("id") == idCateg){
                        ret.put(jpersonObj);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return ret;
    }

    @Override
    public void onResume(){
        super.onResume();
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).registerReceiver(mMessageGetCategorias,
                new IntentFilter("cliente-categorias"));
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).registerReceiver(mMessageGetProductos,
                new IntentFilter("cliente-productos"));

        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).registerReceiver(mMessageWebSocket,
                new IntentFilter("websocket-message"));
    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).unregisterReceiver(mMessageGetCategorias);
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).unregisterReceiver(mMessageGetProductos);

        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).unregisterReceiver(mMessageWebSocket);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).unregisterReceiver(mMessageGetCategorias);
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).unregisterReceiver(mMessageGetProductos);

        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).unregisterReceiver(mMessageWebSocket);
        super.onDestroy();
    }

    private BroadcastReceiver mMessageGetCategorias = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String listaCategorias = intent.getStringExtra("listaCategorias");
            if(listaCategorias.equals("[]")){
                loading.dismiss();
            }else{
                poblarViewPager(listaCategorias);
            }
        }
    };

    private BroadcastReceiver mMessageGetProductos = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String listaProductos = intent.getStringExtra("listaProductos");
            try {
                listProd = new JSONArray(listaProductos);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            new ObtenerCategoriasRequest(getContext()).execute();
        }
    };

    private BroadcastReceiver mMessageWebSocket = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String message = intent.getStringExtra("message");

            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getContext());
            String mail = pref.getString("mail", null);

            new ObtenerProductosRequest(getContext()).execute(mail);

        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_categorias, menu);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        appBarLayout.removeView(tabLayout);
    }

    /**
     * Un {@link FragmentStatePagerAdapter} que gestiona las secciones, fragmentos y
     * títulos de las pestañas
     */
    public class AdaptadorSecciones extends FragmentStatePagerAdapter {
        private final List<Fragment> fragmentos = new ArrayList<>();
        private final List<String> titulosFragmentos = new ArrayList<>();

        public AdaptadorSecciones(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentos.get(position);
        }

        @Override
        public int getCount() {
            return fragmentos.size();
        }

        public void addFragment(Fragment fragment, String title) {
            fragmentos.add(fragment);
            titulosFragmentos.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titulosFragmentos.get(position);
        }
    }
}
