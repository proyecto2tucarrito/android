package com.example.barre.tucarritomobile.Cliente;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.barre.tucarritomobile.CompraRequest.ObtenerComprasRequest;
import com.example.barre.tucarritomobile.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;

/**
 * Created by Barre on 01-Dec-17.
 */
public class FragmentoCompra extends Fragment {
    private AdaptadorCompra adaptador;

    private ListView listView;
    private int ids[];
    private String fechas[];
    private String montos[];
    private String estados[];
    private String monedas[];
    private ProgressDialog loading;

    public FragmentoCompra() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getContext());
        int id = pref.getInt("id", 0);

        if(id != 0)
        {
            new ObtenerComprasRequest(getContext(), "compra").execute(String.valueOf(id));
        }


        View v = inflater.inflate(R.layout.fragmento_compra, container, false);

        listView = (ListView) v.findViewById(R.id.listViewCli);

        loading = new ProgressDialog(v.getContext());
        loading.setMessage("Cargando...");
        loading.setCancelable(false);
        loading.show();

        return v;
    }


    @Override
    public void onResume(){
        super.onResume();
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).registerReceiver(mMessageGetListaCompras,
                new IntentFilter("cliente-compras"));

    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).unregisterReceiver(mMessageGetListaCompras);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).unregisterReceiver(mMessageGetListaCompras);
        super.onDestroy();
    }

    private BroadcastReceiver mMessageGetListaCompras = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String listaCompras = intent.getStringExtra("listaCompras");
            if(listaCompras.equals("[]")){
                loading.dismiss();
            }else{
                showInformation(listaCompras);
            }
        }
    };

    public void showInformation(String listaCompras){
        JSONArray lista = null;
        DecimalFormat df2 = new DecimalFormat(".##");
        try {
            lista = new JSONArray(listaCompras);
            ids = new int [lista.length()];
            fechas = new String [lista.length()];
            montos = new String [lista.length()];
            estados = new String [lista.length()];
            monedas = new String [lista.length()];
            for (int i=0; i < lista.length(); i++) {
                JSONObject jpersonObj = lista.getJSONObject(i);
                ids[i] = jpersonObj.getInt("id");
                fechas[i] = jpersonObj.getString("fecha_alta");
                montos[i] = df2.format(jpersonObj.getDouble("monto_total"));
                estados[i] = jpersonObj.getString("estado");
                monedas[i] = jpersonObj.getJSONArray("lineas_compra").getJSONObject(0).getJSONObject("producto").getString("moneda");

            }
            AdaptadorCompra adaptadorCompra = new AdaptadorCompra(getActivity(), ids, fechas, montos, estados, monedas);
            loading.dismiss();
            listView.setAdapter(adaptadorCompra);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}