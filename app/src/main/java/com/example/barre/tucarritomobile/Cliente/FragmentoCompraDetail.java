package com.example.barre.tucarritomobile.Cliente;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.barre.tucarritomobile.CompraRequest.ObtenerComprasRequest;
import com.example.barre.tucarritomobile.ProductoRequest.ObtenerProductosRequest;
import com.example.barre.tucarritomobile.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;

/**
 * Created by Barre on 01-Dec-17.
 */
public class FragmentoCompraDetail extends Fragment {
    private AdaptadorCompraDetail adaptador;

    private ListView listView;
    private TextView textViewFecha;
    private TextView textViewTotal;
    private TextView textViewEstado;
    private TextView textViewPago;
    private TextView textViewDireccion;
    private static final String ID_COMPRA = "ID_COMPRA";
    private int ids[];
    private String cantidades[];
    private String precios[];
    private String totales[];
    private String monedas[];
    private String nombres[];
    private String descripciones[];
    private String imagenes[];
    private ProgressDialog loading;

    public static FragmentoCompraDetail nuevaInstancia(int idCompra) {
        FragmentoCompraDetail fragment = new FragmentoCompraDetail();

        Bundle args = new Bundle();
        args.putInt(ID_COMPRA, idCompra);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getContext());
        int id = pref.getInt("id", 0);

        if(id != 0)
        {
            new ObtenerComprasRequest(getContext(), "compra_detail").execute(String.valueOf(id));
        }


        View v = inflater.inflate(R.layout.fragmento_compra_detail, container, false);

        listView = (ListView) v.findViewById(R.id.listViewCli);

        textViewFecha = (TextView) v.findViewById(R.id.text_fecha);
        textViewTotal = (TextView) v.findViewById(R.id.text_total);
        textViewEstado = (TextView) v.findViewById(R.id.text_estado);
        textViewPago = (TextView) v.findViewById(R.id.text_pago);
        textViewDireccion = (TextView) v.findViewById(R.id.text_direccion);

        loading = new ProgressDialog(v.getContext());
        loading.setMessage("Cargando...");
        loading.setCancelable(false);
        loading.show();

        return v;
    }


    @Override
    public void onResume(){
        super.onResume();
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).registerReceiver(mMessageGetListaCompras,
                new IntentFilter("cliente-compra-detail"));

    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).unregisterReceiver(mMessageGetListaCompras);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).unregisterReceiver(mMessageGetListaCompras);
        super.onDestroy();
    }

    private BroadcastReceiver mMessageGetListaCompras = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String listaCompras = intent.getStringExtra("listaCompras");
            if(listaCompras.equals("[]")){
                loading.dismiss();
            }else{
                showInformation(listaCompras);
            }
        }
    };

    public void showInformation(String listaCompras){


        int idCompra = getArguments().getInt(ID_COMPRA);

        JSONArray lista = null;
        JSONArray listaLinea = null;
        JSONObject jsonObjectPago = null;
        JSONObject jsonObjectDireccion = null;
        DecimalFormat df2 = new DecimalFormat(".##");
        String moneda = "";
        try {
            lista = new JSONArray(listaCompras);
            for (int i=0; i < lista.length(); i++) {
                JSONObject jpersonObj = lista.getJSONObject(i);

                if(idCompra == jpersonObj.getInt("id"))
                {
                    /*String realizada = "REALIZADA "+jpersonObj.getString("fecha_alta");
                    textViewFecha.setText(realizada);
                    moneda = jpersonObj.getJSONArray("lineas_compra").getJSONObject(0).getJSONObject("producto").getString("moneda");
                    String total = "TOTAL "+moneda+df2.format(jpersonObj.getDouble("monto_total"));
                    textViewTotal.setText(total);
                    String estado = "ESTADO"+jpersonObj.getString("estado");
                    textViewEstado.setText(estado);
                    jsonObjectPago = jpersonObj.getJSONObject("pago");
                    String pago = "PAGO "+jsonObjectPago.getString("tarjeta")+" Cuota"+String.valueOf(jsonObjectPago.getInt("cuota"));
                    textViewPago.setText(pago);
                    jsonObjectDireccion = jpersonObj.getJSONObject("cliente").getJSONObject("direccion");

                    String envio = "";
                    if(jsonObjectDireccion.getString("apto").equals("") || jsonObjectDireccion.getString("apto").equals("null") ||
                            jsonObjectDireccion.getString("apto").equals(null))
                    {
                        envio = "ENVIAR A "+jsonObjectDireccion.getString("calle")+" "+jsonObjectDireccion.getString("numero");
                        textViewDireccion.setText(envio);
                    }
                    else
                    {
                        envio = "ENVIAR A "+jsonObjectDireccion.getString("calle")+" "+jsonObjectDireccion.getString("numero")+
                                " "+jsonObjectDireccion.getString("apto");
                        textViewDireccion.setText(envio);
                    }*/

                    textViewFecha.setText(jpersonObj.getString("fecha_alta"));
                    moneda = jpersonObj.getJSONArray("lineas_compra").getJSONObject(0).getJSONObject("producto").getString("moneda");
                    textViewTotal.setText(moneda+df2.format(jpersonObj.getDouble("monto_total")));
                    textViewEstado.setText(jpersonObj.getString("estado"));
                    jsonObjectPago = jpersonObj.getJSONObject("pago");
                    String pago = "Tarjeta: "+jsonObjectPago.getString("tarjeta")+" Cuota: "+String.valueOf(jsonObjectPago.getInt("cuota"));
                    textViewPago.setText(pago);
                    jsonObjectDireccion = jpersonObj.getJSONObject("cliente").getJSONObject("direccion");

                    String envio = "";
                    if(jsonObjectDireccion.getString("apto").equals("") || jsonObjectDireccion.getString("apto").equals("null") ||
                            jsonObjectDireccion.getString("apto").equals(null))
                    {
                        envio = jsonObjectDireccion.getString("calle")+" "+jsonObjectDireccion.getString("numero");
                        textViewDireccion.setText(envio);
                    }
                    else
                    {
                        envio = jsonObjectDireccion.getString("calle")+" "+jsonObjectDireccion.getString("numero")+
                                " "+jsonObjectDireccion.getString("apto");
                        textViewDireccion.setText(envio);
                    }

                    listaLinea = jpersonObj.getJSONArray("lineas_compra");

                    ids = new int [listaLinea.length()];
                    cantidades = new String [listaLinea.length()];
                    precios = new String [listaLinea.length()];
                    totales = new String [listaLinea.length()];
                    monedas = new String [listaLinea.length()];
                    nombres = new String [listaLinea.length()];
                    descripciones = new String [listaLinea.length()];
                    imagenes = new String [listaLinea.length()];

                    for (int j=0; j < listaLinea.length(); j++) {
                        JSONObject jpersonObjLinea = listaLinea.getJSONObject(j);

                        ids[j] = jpersonObjLinea.getInt("id");
                        cantidades[j] = String.valueOf(jpersonObjLinea.getInt("cantidad"));
                        precios[j] = df2.format(jpersonObjLinea.getDouble("precio"));
                        totales[j] = df2.format(jpersonObjLinea.getDouble("precio") * jpersonObjLinea.getInt("cantidad"));
                        monedas[j] = jpersonObjLinea.getJSONObject("producto").getString("moneda");
                        nombres[j] = jpersonObjLinea.getJSONObject("producto").getString("nombre");
                        descripciones[j] = jpersonObjLinea.getJSONObject("producto").getString("descripcion");
                        imagenes[j] = jpersonObjLinea.getJSONObject("producto").getJSONArray("imagenes").getJSONObject(0).getString("imagen");
                    }
                }
            }
            AdaptadorCompraDetail adaptadorCompraDetail = new AdaptadorCompraDetail(getActivity(), ids, cantidades, precios, totales, monedas,
                    nombres, descripciones, imagenes);
            loading.dismiss();
            listView.setAdapter(adaptadorCompraDetail);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


}
