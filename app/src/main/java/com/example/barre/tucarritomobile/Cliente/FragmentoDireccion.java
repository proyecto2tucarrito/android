package com.example.barre.tucarritomobile.Cliente;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.barre.tucarritomobile.ClienteRequest.UpdateDireccionClienteRequest;
import com.example.barre.tucarritomobile.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Barre on 17-Nov-17.
 */
public class FragmentoDireccion extends Fragment {

    private ProgressDialog loading;

    public FragmentoDireccion() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = null;

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getContext());
        final String direccion = pref.getString("direccion", null);

        if(direccion != null){
            if(!direccion.equals("")){
                v = inflater.inflate(R.layout.fragmento_direccion, container, false);

                GridLayout gridLayoutEdit = (GridLayout) v.findViewById(R.id.grid_edit);
                GridLayout gridLayoutView = (GridLayout) v.findViewById(R.id.grid_view);

                gridLayoutEdit.setVisibility(View.GONE);
                gridLayoutView.setVisibility(View.VISIBLE);

                JSONObject jsonObjectDir = null;
                try {
                    jsonObjectDir = new JSONObject(direccion);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if(jsonObjectDir != null) {
                    TextView textViewCalle = (TextView) v.findViewById(R.id.texto_calle);
                    TextView textViewApto = (TextView) v.findViewById(R.id.texto_apto);
                    TextView textViewApto_txt = (TextView) v.findViewById(R.id.texto_apto_txt);
                    TextView textViewNumero = (TextView) v.findViewById(R.id.texto_numero);
                    TextView textViewPais = (TextView) v.findViewById(R.id.texto_pais);
                    TextView textViewCiudad = (TextView) v.findViewById(R.id.texto_ciudad);
                    TextView textViewDepto = (TextView) v.findViewById(R.id.texto_departamento);

                    final Button bEditar = (Button) v.findViewById(R.id.bEdit);

                    try {
                        textViewCalle.setText(jsonObjectDir.getString("calle"));
                        //String dd = jsonObjectDir.getString("apto");

                        if(jsonObjectDir.getString("apto").equals("null") || jsonObjectDir.getString("apto").equals(null) ||
                                jsonObjectDir.getString("apto").equals("")){
                            textViewApto_txt.setVisibility(View.GONE);
                            textViewApto.setVisibility(View.GONE);
                        }else{
                            textViewApto.setText(jsonObjectDir.getString("apto"));
                        }
                        textViewNumero.setText(jsonObjectDir.getString("numero"));
                        textViewPais.setText(jsonObjectDir.getString("pais"));
                        textViewCiudad.setText(jsonObjectDir.getString("ciudad"));
                        textViewDepto.setText(jsonObjectDir.getString("departamento"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    bEditar.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            GridLayout gridLayoutEdit = (GridLayout) view.getRootView().findViewById(R.id.grid_edit);
                            GridLayout gridLayoutView = (GridLayout) view.getRootView().findViewById(R.id.grid_view);

                            gridLayoutEdit.setVisibility(View.VISIBLE);
                            gridLayoutView.setVisibility(View.GONE);

                            JSONObject jsonObjectDir = null;
                            try {
                                jsonObjectDir = new JSONObject(direccion);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            final EditText etCalle = (EditText) view.getRootView().findViewById(R.id.etCalle);
                            final EditText etApto = (EditText) view.getRootView().findViewById(R.id.etApto);
                            final EditText etNumero = (EditText) view.getRootView().findViewById(R.id.etNumero);
                            final EditText etCiudad = (EditText) view.getRootView().findViewById(R.id.etCiudad);
                            final EditText etDepartamento = (EditText) view.getRootView().findViewById(R.id.etDepartamento);
                            final Spinner spinnerTipo = (Spinner) view.getRootView().findViewById(R.id.spinnerTipo);

                            final Button bRegister = (Button) view.getRootView().findViewById(R.id.bRegister);


                            if(jsonObjectDir != null) {

                                try {
                                    etCalle.setText(jsonObjectDir.getString("calle"));

                                    if(!(jsonObjectDir.getString("apto").equals("null") || jsonObjectDir.getString("apto").equals(null) ||
                                            jsonObjectDir.getString("apto").equals(""))){
                                        etApto.setText(jsonObjectDir.getString("apto"));
                                    }
                                    etNumero.setText(jsonObjectDir.getString("numero"));
                                    ArrayAdapter selSpinner = (ArrayAdapter)spinnerTipo.getAdapter();
                                    spinnerTipo.setSelection(selSpinner.getPosition(jsonObjectDir.getString("pais")));
                                    etCiudad.setText(jsonObjectDir.getString("ciudad"));
                                    etDepartamento.setText(jsonObjectDir.getString("departamento"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }

                            bRegister.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    if(etCalle.getText().toString().trim().equals("") || etNumero.getText().toString().trim().equals("") ||
                                            etCiudad.getText().toString().trim().equals("") || etDepartamento.getText().toString().trim().equals("")){
                                        Toast toast = Toast.makeText(getContext(), "Datos requeridos", Toast.LENGTH_SHORT);
                                        toast.show();
                                    }else{
                                        loading = new ProgressDialog(view.getContext());
                                        loading.setMessage("Guardando...");
                                        loading.setCancelable(false);
                                        loading.show();

                                        guardarDireccion(etCalle.getText().toString(), etApto.getText().toString(), etNumero.getText().toString(),
                                                spinnerTipo.getSelectedItem().toString(), etCiudad.getText().toString(), etDepartamento.getText().toString());
                                    }
                                }
                            });
                        }
                    });

                }
            }
            else{
                //v = inflater.inflate(R.layout.fragmento_direccion_edit, container, false);
                v = inflater.inflate(R.layout.fragmento_direccion, container, false);

                GridLayout gridLayoutEdit = (GridLayout) v.findViewById(R.id.grid_edit);
                GridLayout gridLayoutView = (GridLayout) v.findViewById(R.id.grid_view);

                gridLayoutEdit.setVisibility(View.VISIBLE);
                gridLayoutView.setVisibility(View.GONE);

                final EditText etCalle = (EditText) v.findViewById(R.id.etCalle);
                final EditText etApto = (EditText) v.findViewById(R.id.etApto);
                final EditText etNumero = (EditText) v.findViewById(R.id.etNumero);
                final EditText etCiudad = (EditText) v.findViewById(R.id.etCiudad);
                final EditText etDepartamento = (EditText) v.findViewById(R.id.etDepartamento);
                final Spinner spinnerTipo = (Spinner) v.findViewById(R.id.spinnerTipo);

                final Button bRegister = (Button) v.findViewById(R.id.bRegister);

                bRegister.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if(etCalle.getText().toString().trim().equals("") || etNumero.getText().toString().trim().equals("") ||
                                etCiudad.getText().toString().trim().equals("") || etDepartamento.getText().toString().trim().equals("")){
                            Toast toast = Toast.makeText(getContext(), "Datos requeridos", Toast.LENGTH_SHORT);
                            toast.show();
                        }else{
                            loading = new ProgressDialog(view.getContext());
                            loading.setMessage("Guardando...");
                            loading.setCancelable(false);
                            loading.show();

                            guardarDireccion(etCalle.getText().toString(), etApto.getText().toString(), etNumero.getText().toString(),
                                    spinnerTipo.getSelectedItem().toString(), etCiudad.getText().toString(), etDepartamento.getText().toString());
                        }
                    }
                });
            }
        }


        return v;
    }

    private void guardarDireccion(String calle, String apto, String numero, String pais, String ciudad, String depto){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getContext());
        String mail = pref.getString("mail", null);
        new UpdateDireccionClienteRequest(getContext(), loading).execute(mail, calle, apto, numero, pais, ciudad, depto);
    }

}