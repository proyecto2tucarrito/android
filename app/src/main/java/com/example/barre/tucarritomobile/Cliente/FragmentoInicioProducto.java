package com.example.barre.tucarritomobile.Cliente;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.barre.tucarritomobile.ProductoRequest.ObtenerProductosRequest;
import com.example.barre.tucarritomobile.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;

/**
 * Created by Barre on 15-Nov-17.
 */
public class FragmentoInicioProducto extends Fragment {

    private ListView listView;
    private int ids[];
    private String names[];
    private String desc[];
    private String precio[];
    private String imageid[];
    private String stocks[];
    private String marcas[];
    private ProgressDialog loading;


    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getContext());
        String mail = pref.getString("mail", null);

        new ObtenerProductosRequest(getContext()).execute(mail);

        View v = inflater.inflate(R.layout.fragmento_inicio_producto, container, false);

        listView = (ListView) v.findViewById(R.id.listViewCli);

        loading = new ProgressDialog(v.getContext());
        loading.setMessage("Cargando...");
        loading.setCancelable(false);
        loading.show();

        return v;
    }

    @Override
    public void onResume(){
        super.onResume();
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).registerReceiver(mMessageGetListaProductos,
                new IntentFilter("cliente-productos"));
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).registerReceiver(mMessageWebSocket,
                new IntentFilter("websocket-message"));

    }


    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).unregisterReceiver(mMessageGetListaProductos);
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).unregisterReceiver(mMessageWebSocket);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).unregisterReceiver(mMessageGetListaProductos);
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).unregisterReceiver(mMessageWebSocket);
        super.onDestroy();
    }

    private BroadcastReceiver mMessageGetListaProductos = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String listaProductos = intent.getStringExtra("listaProductos");
            Log.v("LISTASOL", listaProductos);
            if(listaProductos.equals("[]")){
                loading.dismiss();
            }else{
                showInformation(listaProductos);
            }
        }
    };

    private BroadcastReceiver mMessageWebSocket = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String message = intent.getStringExtra("message");

            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getContext());
            String mail = pref.getString("mail", null);

            new ObtenerProductosRequest(getContext()).execute(mail);

        }
    };

    public void showInformation(String listaProductos){
        JSONArray lista = null;
        JSONArray listaimg = null;
        DecimalFormat df2 = new DecimalFormat(".##");
        try {
            lista = new JSONArray(listaProductos);
            ids = new int [lista.length()];
            names = new String [lista.length()];
            desc = new String [lista.length()];
            precio = new String [lista.length()];
            imageid = new String [lista.length()];
            stocks = new String [lista.length()];
            marcas = new String [lista.length()];
            for (int i=0; i < lista.length(); i++) {
                JSONObject jpersonObj = lista.getJSONObject(i);
                ids[i] = jpersonObj.getInt("id");
                names[i] = jpersonObj.getString("nombre");
                desc[i] = jpersonObj.getString("descripcion");
                /*if(df2.format(jpersonObj.getDouble("descuento")).equals(0.00))
                {

                }*/
                precio[i] = jpersonObj.getString("moneda")+String.valueOf(df2.format(jpersonObj.getDouble("precioConDcto")));

                listaimg = jpersonObj.getJSONArray("imagenes");
                JSONObject jsonImage = listaimg.getJSONObject(0);
                imageid[i] = jsonImage.getString("imagen").replaceAll("\\'","'");
                stocks[i] = "Hay "+String.valueOf(jpersonObj.getInt("stock"))+" en stock.";
                JSONObject jsonMarca = jpersonObj.getJSONObject("marca");
                marcas[i] = jsonMarca.getString("nombre");
            }
            AdaptadorInicioProducto adaptadorInicioProducto = new AdaptadorInicioProducto(getActivity(), ids, names, desc, precio, imageid, stocks, marcas, lista);
            loading.dismiss();
            listView.setAdapter(adaptadorInicioProducto);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}
