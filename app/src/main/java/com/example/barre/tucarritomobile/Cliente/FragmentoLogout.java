package com.example.barre.tucarritomobile.Cliente;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.barre.tucarritomobile.CarritoApp;
import com.example.barre.tucarritomobile.ClienteRequest.LogoutClienteRequest;
import com.example.barre.tucarritomobile.R;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

/**
 * Created by Barre on 15-Nov-17.
 */
public class FragmentoLogout extends Fragment {

    private ProgressDialog loading;

    public FragmentoLogout() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loading = new ProgressDialog(getActivity());
        loading.setMessage("Cerrando sesion...");
        loading.setCancelable(false);
        loading.show();
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getContext());
        String red = pref.getString("Red", null);

        if(red.equals("Facebook")){
            logoutSendRequest();
        }else{
            signOut();
        }
    }

    public void logoutSendRequest(){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getContext());
        String mail = pref.getString("mail", null);

        if(mail != null){
            new LogoutClienteRequest(getContext(), getActivity(), loading).execute(mail);
        }
    }

    private void signOut() {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getContext());
        final String mail = pref.getString("mail", null);
        final CarritoApp carritoApp = (CarritoApp) getContext().getApplicationContext();
        carritoApp.mGoogleApiClient.connect();
        carritoApp.mGoogleApiClient.registerConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
            @Override
            public void onConnected(@Nullable Bundle bundle) {
                if(carritoApp.mGoogleApiClient.isConnected()) {
                    Auth.GoogleSignInApi.signOut(carritoApp.mGoogleApiClient).setResultCallback(new ResultCallback<Status>() {
                        @Override
                        public void onResult(@NonNull Status status) {
                            if (status.isSuccess()) {
                                if(mail != null){
                                    new LogoutClienteRequest(getContext(), getActivity(), loading).execute(mail);
                                }
                            }
                        }
                    });
                }
            }

            @Override
            public void onConnectionSuspended(int i) {
                Log.d("SUS", "Google API Client Connection Suspended");
            }
        });

    }

}
