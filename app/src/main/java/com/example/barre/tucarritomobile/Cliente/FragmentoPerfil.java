package com.example.barre.tucarritomobile.Cliente;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.barre.tucarritomobile.R;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;


/**
 * Fragmento para la pestaña "PERFIL" De la sección "Mi Cuenta"
 */
public class FragmentoPerfil extends Fragment {

    public FragmentoPerfil() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getContext());
        String mail = pref.getString("mail", null);
        String nombre = pref.getString("nombre", null);
        String imagen = pref.getString("imagen", null);

        View v = inflater.inflate(R.layout.fragmento_perfil, container, false);

        TextView textViewName = (TextView) v.findViewById(R.id.texto_nombre);
        TextView textViewEmail = (TextView) v.findViewById(R.id.texto_email);
        ImageView image = (ImageView) v.findViewById(R.id.imageView);

        textViewName.setText(nombre);
        textViewEmail.setText(mail);
        Picasso.with(getContext()).load(imagen).into(image);

        return v;
    }
}
