package com.example.barre.tucarritomobile.Cliente;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.barre.tucarritomobile.CarritoApp;
import com.example.barre.tucarritomobile.Carro.Item;
import com.example.barre.tucarritomobile.CompraRequest.ObtenerComprasRequest;
import com.example.barre.tucarritomobile.ProductoRequest.ObtenerProductoDetailRequest;
import com.example.barre.tucarritomobile.ProductoRequest.ObtenerProductosRequest;
import com.example.barre.tucarritomobile.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import ss.com.bannerslider.banners.Banner;
import ss.com.bannerslider.banners.DrawableBanner;
import ss.com.bannerslider.banners.RemoteBanner;
import ss.com.bannerslider.views.BannerSlider;

/**
 * Created by Barre on 02-Dec-17.
 */
public class FragmentoProductoDetail extends Fragment {

    private BannerSlider bannerSlider;
    private TextView textViewName;
    private TextView textViewDesc;
    private TextView textViewMarca;
    private TextView textViewStock;
    private TextView textViewPrecio;
    private TextView textViewCantidad;
    private ImageView imageCarro;
    private ImageView imageMinus;
    private ImageView imagePlus;
    private static final String ID_PRODUCTO = "ID_PRODUCTO";
    private ProgressDialog loading;
    private CarritoApp carritoApp;

    public static FragmentoProductoDetail nuevaInstancia(int idProducto) {
        FragmentoProductoDetail fragment = new FragmentoProductoDetail();

        Bundle args = new Bundle();
        args.putInt(ID_PRODUCTO, idProducto);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        int idProducto = getArguments().getInt(ID_PRODUCTO);

        if(idProducto != 0)
        {
            new ObtenerProductoDetailRequest(getContext()).execute(String.valueOf(idProducto));
        }

        View v = inflater.inflate(R.layout.fragmento_producto_detail, container, false);

        loading = new ProgressDialog(v.getContext());
        loading.setMessage("Cargando...");
        loading.setCancelable(false);
        loading.show();

        bannerSlider = (BannerSlider) v.findViewById(R.id.banner_slider1);
        textViewName = (TextView) v.findViewById(R.id.textViewName);
        textViewDesc = (TextView) v.findViewById(R.id.textViewDesc);
        textViewPrecio = (TextView) v.findViewById(R.id.textViewPrecio);
        textViewStock = (TextView) v.findViewById(R.id.textViewStock);
        textViewMarca = (TextView) v.findViewById(R.id.textViewMarca);

        imageCarro = (ImageView) v.findViewById(R.id.imageViewCarro);
        imageMinus = (ImageView) v.findViewById(R.id.imageViewMinus);
        imagePlus = (ImageView) v.findViewById(R.id.imageViewPlus);
        textViewCantidad = (TextView) v.findViewById(R.id.textViewCantidad);

        carritoApp = (CarritoApp) getContext().getApplicationContext();


        return v;
    }


    @Override
    public void onResume(){
        super.onResume();
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).registerReceiver(mMessageGetProductoDetail,
                new IntentFilter("cliente-producto-detail"));

        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).registerReceiver(mMessageWebSocket,
                new IntentFilter("websocket-message"));
    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).unregisterReceiver(mMessageGetProductoDetail);
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).unregisterReceiver(mMessageWebSocket);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).unregisterReceiver(mMessageGetProductoDetail);
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).unregisterReceiver(mMessageWebSocket);
        super.onDestroy();
    }

    private BroadcastReceiver mMessageWebSocket = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String message = intent.getStringExtra("message");

            int idProducto = getArguments().getInt(ID_PRODUCTO);
            new ObtenerProductoDetailRequest(getContext()).execute(String.valueOf(idProducto));

        }
    };

    private BroadcastReceiver mMessageGetProductoDetail = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String producto_detail = intent.getStringExtra("producto_detail");
            showInformation(producto_detail);

        }
    };

    public void showInformation(String producto_detail){

        JSONObject jpersonObj = null;
        JSONArray jsonArrayImg = null;
        DecimalFormat df2 = new DecimalFormat(".##");
        try {

            jpersonObj = new JSONObject(producto_detail);

            imageCarro.setTag(jpersonObj.toString());
            imageMinus.setTag(jpersonObj.toString());
            imagePlus.setTag(jpersonObj.toString());

            textViewName.setText(jpersonObj.getString("nombre"));
            String textDesc = "";
            String textMarca = "";
            /*try {
                textDesc = new String(jpersonObj.getString("descripcion").getBytes("ISO-8859-1"));
                JSONObject jsonMarca = jpersonObj.getJSONObject("marca");
                textMarca = new String(jsonMarca.getString("nombre").getBytes("ISO-8859-1"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }*/
            textDesc = jpersonObj.getString("descripcion");
            JSONObject jsonMarca = jpersonObj.getJSONObject("marca");
            textMarca = jsonMarca.getString("nombre");

            textViewDesc.setText(textDesc);
            String precio = jpersonObj.getString("moneda")+String.valueOf(df2.format(jpersonObj.getDouble("precioConDcto")));
            textViewPrecio.setText(precio);
            textViewMarca.setText(textMarca);
            String stock = "Hay "+String.valueOf(jpersonObj.getInt("stock"))+" en stock.";
            textViewStock.setText(stock);

            jsonArrayImg = jpersonObj.getJSONArray("imagenes");
            List<Banner> banners=new ArrayList<>();

            for (int i=0; i < jsonArrayImg.length(); i++) {
                JSONObject jsonObject = jsonArrayImg.getJSONObject(i);

                String url = jsonObject.getString("imagen");
                banners.add(new RemoteBanner(url));
            }

            bannerSlider.setBanners(banners);

            if(carritoApp.carro.getList().size() == 0)
            {
                imageCarro.setVisibility(View.VISIBLE);
                imagePlus.setVisibility(View.GONE);
                imageMinus.setVisibility(View.GONE);
                textViewCantidad.setVisibility(View.GONE);
            }
            else
            {
                imageCarro.setVisibility(View.VISIBLE);
                imagePlus.setVisibility(View.GONE);
                imageMinus.setVisibility(View.GONE);
                textViewCantidad.setVisibility(View.GONE);

                int idProducto = getArguments().getInt(ID_PRODUCTO);
                ListIterator<Item> iter = carritoApp.carro.getList().listIterator();
                while(iter.hasNext()){
                    Item item = (Item) iter.next();

                    try {
                        if(item.getProducto().getInt("id") == idProducto) {
                            imageCarro.setVisibility(View.GONE);
                            imagePlus.setVisibility(View.VISIBLE);
                            imageMinus.setVisibility(View.VISIBLE);
                            textViewCantidad.setVisibility(View.VISIBLE);
                            textViewCantidad.setText(String.valueOf(item.getCantidad()));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            imageCarro.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String tag = (String) view.getTag();
                    try {
                        JSONObject newJsonObject = new JSONObject(tag);

                        if(newJsonObject.getInt("stock") != 0)
                        {
                            //Agrego al carro
                            carritoApp.carro.addItem(newJsonObject);

                            //Cambio las imagenes
                            imageCarro.setVisibility(View.GONE);
                            imagePlus.setVisibility(View.VISIBLE);
                            imageMinus.setVisibility(View.VISIBLE);
                            textViewCantidad.setVisibility(View.VISIBLE);

                            //Envio a los demas clientes
                            String id = String.valueOf(newJsonObject.getInt("id"));
                            String numberCant = String.valueOf(1);
                            Intent intent = new Intent("cliente-sendMessage");
                            intent.putExtra("message", id+","+numberCant+",-");
                            LocalBroadcastManager.getInstance(getContext().getApplicationContext()).sendBroadcast(intent);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });

            imageMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String tag = (String) view.getTag();
                    try {
                        //Remuevo del carro
                        JSONObject newJsonObject = new JSONObject(tag);
                        carritoApp.carro.removeItem(newJsonObject);

                        //Cambia imagenes y cantidad
                        if(textViewCantidad.getText().toString().equals("1"))
                        {
                            imageCarro.setVisibility(View.VISIBLE);
                            imagePlus.setVisibility(View.GONE);
                            imageMinus.setVisibility(View.GONE);
                            textViewCantidad.setVisibility(View.GONE);
                        }
                        else
                        {
                            imageCarro.setVisibility(View.GONE);
                            imagePlus.setVisibility(View.VISIBLE);
                            imageMinus.setVisibility(View.VISIBLE);
                            textViewCantidad.setVisibility(View.VISIBLE);
                            int cant = Integer.parseInt(textViewCantidad.getText().toString());
                            textViewCantidad.setText(String.valueOf(cant-1));
                        }

                        //Envio a los demas clientes
                        String id = String.valueOf(newJsonObject.getInt("id"));
                        String numberCant = String.valueOf(1);
                        Intent intent = new Intent("cliente-sendMessage");
                        intent.putExtra("message", id+","+numberCant+",+");
                        LocalBroadcastManager.getInstance(getContext().getApplicationContext()).sendBroadcast(intent);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });

            imagePlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String tag = (String) view.getTag();
                    try {
                        JSONObject newJsonObject = new JSONObject(tag);

                        if(newJsonObject.getInt("stock") != 0)
                        {
                            //Agrego al carro
                            carritoApp.carro.addItem(newJsonObject);

                            //Cambio cantidad
                            int cant = Integer.parseInt(textViewCantidad.getText().toString());
                            textViewCantidad.setText(String.valueOf(cant + 1));

                            //Envio a los demas clientes
                            String id = String.valueOf(newJsonObject.getInt("id"));
                            String numberCant = String.valueOf(1);
                            Intent intent = new Intent("cliente-sendMessage");
                            intent.putExtra("message", id + "," + numberCant + ",-");
                            LocalBroadcastManager.getInstance(getContext().getApplicationContext()).sendBroadcast(intent);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });

            loading.dismiss();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


}
