package com.example.barre.tucarritomobile.Cliente;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.barre.tucarritomobile.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;

/**
 * Created by Barre on 20-Nov-17.
 */
public class FragmentoWebView extends Fragment {

    private static final String URL  = "test";

    public static FragmentoWebView nuevaInstancia(String url) {
        FragmentoWebView fragment = new FragmentoWebView();

        Bundle args = new Bundle();
        args.putString(URL, url);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragmento_webview, container, false);

        String url = getArguments().getString(URL);
        WebView myWebView = (WebView) v.findViewById(R.id.webView);

        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.getSettings().setBuiltInZoomControls(true);
        //Cargamos el enlace definido
        myWebView.loadUrl(url);
        //Este método es para que el navegador se quede en nuestra aplicación
        myWebView.setWebViewClient(new WebViewClient());
                /*myWebView.setWebViewClient(new WebViewClient(){
                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                        return false;
                    }
                });*/

        return v;
    }
}
