package com.example.barre.tucarritomobile.ClienteRequest;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

import com.example.barre.tucarritomobile.LoginActivity;
import com.example.barre.tucarritomobile.Util;
import com.facebook.login.LoginManager;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

/**
 * Created by Martin on 16/11/2016.
 */
public class LogoutClienteRequest extends AsyncTask<String, Integer, Boolean> {

    private Context context;
    private Activity activity;
    private ProgressDialog loading;
    public LogoutClienteRequest(Context context, Activity activity, ProgressDialog loading) {
        this.context = context.getApplicationContext();
        this.activity = activity;
        this.loading = loading;
    }

    /*@Override
    protected Boolean doInBackground(String... params) {

        Boolean result = false;
        JSONObject jsonObject = null;
        HttpClient httpClient = new DefaultHttpClient();

        try {
            String appName = Util.getProperty("Nombre", context);
            HttpPut httpPut = new HttpPut("http://" + Util.getProperty("IP", context) + ":8080/tuCarrito-web/rest/cliente/logoutMobile/" + appName);
            httpPut.setHeader("content-type", "application/json");
            httpPut.setHeader("accept", "application/json");

            jsonObject = new JSONObject();
            jsonObject.put("mail", params[0]);

            StringEntity entity = new StringEntity(jsonObject.toString());
            httpPut.setEntity(entity);

            HttpResponse httpResponse = httpClient.execute(httpPut);
            if (httpResponse.getStatusLine().getStatusCode() == 200) {
                result = true;
            }
        } catch (Exception e) {

        }
        return result;
    }*/

    @Override
    protected Boolean doInBackground(String... params) {

        Boolean result = false;
        JSONObject jsonObject = null;

        try {
            String appName = Util.getProperty("Nombre", context);

            URL url = new URL("https://" + Util.getProperty("IP", context) + ":8443/tuCarrito-web/rest/cliente/logoutMobile/" + appName);

            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier(){
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }});
            SSLContext contextSSL = SSLContext.getInstance("TLS");
            contextSSL.init(null, new X509TrustManager[]{new X509TrustManager(){
                public void checkClientTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {}
                public void checkServerTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {}
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }}}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(
                    contextSSL.getSocketFactory());

            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

            conn.setDoOutput(true);
            conn.setRequestMethod("PUT");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("accept", "application/json");

            jsonObject = new JSONObject();
            jsonObject.put("mail", params[0]);

            String input = jsonObject.toString();
            OutputStream os = conn.getOutputStream();
            os.write(input.getBytes());
            os.flush();
            os.close();
            Long resultCode = (long) conn.getResponseCode();

            if(resultCode == 200){
                result = true;
            }

        } catch (Exception e) {

        }
        return result;
    }

    protected void onPostExecute(Boolean result) {
        if(result){
            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
            String red = pref.getString("Red", null);

            if(red.equals("Facebook")){
                LoginManager.getInstance().logOut();
            }
            if((loading != null) && loading.isShowing()){
                loading.dismiss();
            }
            Intent intent = new Intent(activity, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);

            /*String url = "https://www.amazon.es";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            i.setData(Uri.parse(url));
            context.startActivity(i);*/

        }

    }
}
