package com.example.barre.tucarritomobile.ClienteRequest;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import com.example.barre.tucarritomobile.CarritoApp;
import com.example.barre.tucarritomobile.Cliente.ClienteAreaActivity;
import com.example.barre.tucarritomobile.LoginActivity;
import com.example.barre.tucarritomobile.Util;
import com.facebook.Profile;
import com.google.gson.Gson;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

/**
 * Created by Barre on 06-Nov-17.
 */
public class RegisterClienteRequest extends AsyncTask<String, Integer, JSONObject> {

    private String red;
    private CarritoApp carritoApp;
    private Context context;
    private ProgressDialog loading;
    /*public RegisterClienteRequest(Context context, ProgressDialog loading) {
        this.context = context.getApplicationContext();
        this.loading = loading;
    }*/
    public RegisterClienteRequest(Context context) {
        this.context = context.getApplicationContext();
    }
    /*
    @Override
    protected JSONObject doInBackground(String... params){
        //Boolean result = true;
        JSONObject jsonObject = null;
        HttpClient httpClient = new DefaultHttpClient();

        try{
            String appName = Util.getProperty("Nombre", context);
            HttpPost httpPost = new HttpPost("http://"+ Util.getProperty("IP", context)+":8080/tuCarrito-web/rest/cliente/registroMobile/"+appName);
            httpPost.setHeader("content-type", "application/json");
            httpPost.setHeader("accept", "application/json");

            String nombre = "";
            String pic = "";

            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
            red = pref.getString("Red", null);
            if(red.equals("Facebook")){
                Profile profile = Profile.getCurrentProfile();

                nombre = profile.getName();
                pic = profile.getProfilePictureUri(200,200).toString();
            }
            else{
                carritoApp = (CarritoApp) context;
                nombre = carritoApp.acct.getDisplayName();
                pic = carritoApp.acct.getPhotoUrl().toString();
            }

            jsonObject = new JSONObject();
            jsonObject.put("nombre", nombre);
            jsonObject.put("imagen", pic);
            jsonObject.put("mail", params[0]);
            jsonObject.put("password", params[0]);
            jsonObject.put("activo", true);

            StringEntity entity = new StringEntity(jsonObject.toString());
            httpPost.setEntity(entity);

            HttpResponse httpResponse = httpClient.execute(httpPost);
            if(httpResponse.getStatusLine().getStatusCode() == 200)
            {
                String json = EntityUtils.toString(httpResponse.getEntity());
                JSONObject jsonObjectCli = new JSONObject(json);
                return jsonObjectCli;
            }
            else {
                return null;
            }
        }catch (Exception e){
            return null;
        }
    }
*/

    @Override
    protected JSONObject doInBackground(String... params){
        //Boolean result = true;
        JSONObject jsonObject = null;

        try{
            String appName = Util.getProperty("Nombre", context);
            String locationUrl = Util.getProperty("LocationUrl", context);

            URL url = new URL("https://"+ Util.getProperty("IP", context)+":8443/tuCarrito-web/rest/cliente/registroMobile/"+appName);

            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier(){
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }});
            SSLContext contextSSL = SSLContext.getInstance("TLS");
            contextSSL.init(null, new X509TrustManager[]{new X509TrustManager(){
                public void checkClientTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {}
                public void checkServerTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {}
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }}}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(
                    contextSSL.getSocketFactory());

            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("accept", "application/json");

            String nombre = "";
            String pic = "";

            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
            red = pref.getString("Red", null);
            if(red.equals("Facebook")){
                Profile profile = Profile.getCurrentProfile();

                nombre = profile.getName();
                pic = profile.getProfilePictureUri(200,200).toString();
            }
            else{
                carritoApp = (CarritoApp) context;
                nombre = carritoApp.acct.getDisplayName();
                pic = carritoApp.acct.getPhotoUrl().toString();
            }

            jsonObject = new JSONObject();
            jsonObject.put("nombre", nombre);
            jsonObject.put("imagen", pic);
            jsonObject.put("mail", params[0]);
            jsonObject.put("password", params[0]);
            jsonObject.put("activo", true);

            //String input = new Gson().toJson(jsonObject);
            String input = jsonObject.toString();
            OutputStream os = conn.getOutputStream();
            os.write(input.getBytes());
            os.flush();
            os.close();
            Long result = (long) conn.getResponseCode();

            if(result == 200){
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                JSONObject jsonObjectCli = new JSONObject(response.toString());
                return jsonObjectCli;
            }
            else{
                return null;
            }

        }catch (Exception e){
            return null;
        }
    }

    protected void onPostExecute(JSONObject result){

        if(result != null){
            int id = 0;
            String mail = null;
            String nombre = null;
            String imagen = null;
            JSONObject direccion = null;
            try {
                id = result.getInt("id");
                mail = result.getString("mail");
                nombre = result.getString("nombre");
                imagen = result.getString("imagen");
                direccion = result.getJSONObject("direccion");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            /*if((loading != null) && loading.isShowing()){
                loading.dismiss();
            }*/
            Intent intent = new Intent(context, ClienteAreaActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("id", id);
            intent.putExtra("mail", mail);
            intent.putExtra("nombre", nombre);
            intent.putExtra("imagen", imagen);
            if(direccion != null){
                intent.putExtra("direccion", direccion.toString());
            }
            else{
                intent.putExtra("direccion", "");
            }

            context.startActivity(intent);
        }
        else{
            Intent intent = new Intent(context, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }

    }

}
