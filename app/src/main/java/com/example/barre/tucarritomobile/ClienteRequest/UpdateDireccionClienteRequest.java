package com.example.barre.tucarritomobile.ClienteRequest;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.Toast;

import com.example.barre.tucarritomobile.Util;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

/**
 * Created by Barre on 18-Nov-17.
 */
public class UpdateDireccionClienteRequest extends AsyncTask<String, Integer, JSONObject> {

    private Context context;
    private ProgressDialog loading;
    public UpdateDireccionClienteRequest(Context context, ProgressDialog loading) {
        this.context = context.getApplicationContext();
        this.loading = loading;
    }
    /*@Override
    protected JSONObject doInBackground(String... params){

        JSONObject jsonObject = null;
        JSONObject jsonObjectDireccion = null;
        HttpClient httpClient = new DefaultHttpClient();

        try{
            String appName = Util.getProperty("Nombre", context);
            HttpPut httpPut = new HttpPut("http://"+ Util.getProperty("IP", context)+":8080/tuCarrito-web/rest/cliente/updateDireccion/"+appName);
            httpPut.setHeader("content-type", "application/json");
            httpPut.setHeader("accept", "application/json");

            jsonObject = new JSONObject();
            jsonObjectDireccion = new JSONObject();

            jsonObjectDireccion.put("calle", params[1]);
            jsonObjectDireccion.put("apto", params[2]);
            jsonObjectDireccion.put("numero", params[3]);
            jsonObjectDireccion.put("pais", params[4]);
            jsonObjectDireccion.put("ciudad", params[5]);
            jsonObjectDireccion.put("departamento", params[6]);

            jsonObject.put("mail", params[0]);
            jsonObject.put("direccion", jsonObjectDireccion);

            StringEntity entity = new StringEntity(jsonObject.toString());
            httpPut.setEntity(entity);

            HttpResponse httpResponse = httpClient.execute(httpPut);
            if(httpResponse.getStatusLine().getStatusCode() == 200)
            {
                return jsonObject;
            }
            else {
                return null;
            }
        }catch (Exception e){
            return null;
        }

    }*/

    @Override
    protected JSONObject doInBackground(String... params){

        JSONObject jsonObject = null;
        JSONObject jsonObjectDireccion = null;

        try{
            String appName = Util.getProperty("Nombre", context);

            URL url = new URL("https://"+ Util.getProperty("IP", context)+":8443/tuCarrito-web/rest/cliente/updateDireccion/"+appName);

            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier(){
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }});
            SSLContext contextSSL = SSLContext.getInstance("TLS");
            contextSSL.init(null, new X509TrustManager[]{new X509TrustManager(){
                public void checkClientTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {}
                public void checkServerTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {}
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }}}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(
                    contextSSL.getSocketFactory());

            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

            conn.setDoOutput(true);
            conn.setRequestMethod("PUT");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("accept", "application/json");

            jsonObject = new JSONObject();
            jsonObjectDireccion = new JSONObject();

            jsonObjectDireccion.put("calle", params[1]);
            jsonObjectDireccion.put("apto", params[2]);
            jsonObjectDireccion.put("numero", params[3]);
            jsonObjectDireccion.put("pais", params[4]);
            jsonObjectDireccion.put("ciudad", params[5]);
            jsonObjectDireccion.put("departamento", params[6]);

            jsonObject.put("mail", params[0]);
            jsonObject.put("direccion", jsonObjectDireccion);

            String input = jsonObject.toString();
            OutputStream os = conn.getOutputStream();
            os.write(input.getBytes());
            os.flush();
            os.close();
            Long result = (long) conn.getResponseCode();

            if(result == 200){
                return jsonObject;
            }
            else{
                return null;
            }
        }catch (Exception e){
            return null;
        }

    }

    protected void onPostExecute(JSONObject result){
        if(result != null){
            JSONObject dir = null;

            try {
                dir = result.getJSONObject("direccion");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if(dir != null) {
                SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
                SharedPreferences.Editor editor = pref.edit();

                editor.putString("direccion", dir.toString());
                editor.putBoolean("editarDir", false);
                editor.commit();
            }

            if((loading != null) && loading.isShowing()){
                loading.dismiss();
            }

            Intent intent = new Intent("cliente-direccion");
            LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(intent);
            //FragmentTransaction ft = getFragmentManager().beginTransaction();
            //ft.detach(this).attach(this).commit();
        }
        else{

            if((loading != null) && loading.isShowing()){
                loading.dismiss();
            }

            Toast toast = Toast.makeText(context.getApplicationContext(), "Error", Toast.LENGTH_SHORT);
            toast.show();
        }


    }

}