package com.example.barre.tucarritomobile.ClienteRequest;

import android.content.Context;
import android.os.AsyncTask;


import com.example.barre.tucarritomobile.Util;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.OutputStream;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

/**
 * Created by Martin on 26/10/2016.
 */
public class UpdateTokenClienteRequest extends AsyncTask<String, Integer, Boolean> {

    private Context context;
    public UpdateTokenClienteRequest(Context context) {
        this.context = context.getApplicationContext();
    }
    /*@Override
    protected Boolean doInBackground(String... params){

        Boolean result = false;
        JSONObject jsonObject = null;
        HttpClient httpClient = new DefaultHttpClient();

        try{
            String appName = Util.getProperty("Nombre", context);
            HttpPut httpPut = new HttpPut("http://"+ Util.getProperty("IP", context)+":8080/tuCarrito-web/rest/cliente/updateToken/"+appName+"/"+params[2]);
            httpPut.setHeader("content-type", "application/json");
            httpPut.setHeader("accept", "application/json");

            jsonObject = new JSONObject();
            jsonObject.put("tokenPush", params[0]);
            jsonObject.put("idFirebase", params[1]);

            StringEntity entity = new StringEntity(jsonObject.toString());
            httpPut.setEntity(entity);

            HttpResponse httpResponse = httpClient.execute(httpPut);
            if(httpResponse.getStatusLine().getStatusCode() == 200)
            {
                result = true;
            }
        }catch (Exception e){

        }
        return result;
    }*/

    @Override
    protected Boolean doInBackground(String... params){

        Boolean result = false;
        JSONObject jsonObject = null;

        try{
            String appName = Util.getProperty("Nombre", context);

            URL url = new URL("https://"+ Util.getProperty("IP", context)+":8443/tuCarrito-web/rest/cliente/updateToken/"+appName+"/"+params[2]);

            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier(){
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }});
            SSLContext contextSSL = SSLContext.getInstance("TLS");
            contextSSL.init(null, new X509TrustManager[]{new X509TrustManager(){
                public void checkClientTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {}
                public void checkServerTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {}
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }}}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(
                    contextSSL.getSocketFactory());

            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

            conn.setDoOutput(true);
            conn.setRequestMethod("PUT");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("accept", "application/json");

            jsonObject = new JSONObject();
            jsonObject.put("tokenPush", params[0]);
            jsonObject.put("idFirebase", params[1]);

            String input = jsonObject.toString();
            OutputStream os = conn.getOutputStream();
            os.write(input.getBytes());
            os.flush();
            os.close();
            Long resultCode = (long) conn.getResponseCode();

            if(resultCode == 200){
                result = true;
            }
        }catch (Exception e){

        }
        return result;
    }


    protected void onPostExecute(Boolean result){
       /* SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.commit();*/


    }

}
