package com.example.barre.tucarritomobile.CompraRequest;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Base64;

import com.example.barre.tucarritomobile.CarritoApp;
import com.example.barre.tucarritomobile.Carro.Item;
import com.example.barre.tucarritomobile.Util;
import com.google.gson.Gson;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Created by Barre on 20-Nov-17.
 */
public class InsertarCompraPendienteRequest extends AsyncTask<String, Integer, JSONObject> {

    private String mail;
    private Context context;
    private ProgressDialog loading;

    public InsertarCompraPendienteRequest(Context context, ProgressDialog loading) {
        this.context = context.getApplicationContext();
        this.loading = loading;
    }

    public InsertarCompraPendienteRequest(Context context) {
        this.context = context.getApplicationContext();
        //this.loading = loading;
    }


    /*@Override
    protected JSONObject doInBackground(String... params){
        JSONObject jsonObject = null;
        HttpClient httpClient = new DefaultHttpClient();

        try{
            String appName = Util.getProperty("Nombre", context);

            //HttpPost httpPost = new HttpPost("https://"+ Util.getProperty("IP_PASARELA", context)+":8443/pasarela-web/rest/compra/compraDatos");
            HttpPost httpPost = new HttpPost("http://"+ Util.getProperty("IP_PASARELA", context)+":8080/pasarela-web/rest/compra/compraDatos");
            httpPost.setHeader("content-type", "application/json");
            httpPost.setHeader("accept", "application/json");


            jsonObject = new JSONObject();
            jsonObject.put("numOrden", params[0]);
            jsonObject.put("empresa", appName);
            jsonObject.put("monto", params[1]);
            jsonObject.put("url", "");

            StringEntity entity = new StringEntity(jsonObject.toString());
            httpPost.setEntity(entity);

            HttpResponse httpResponse = httpClient.execute(httpPost);
            if(httpResponse.getStatusLine().getStatusCode() == 200)
            {
                String json = EntityUtils.toString(httpResponse.getEntity());
                JSONObject jsonObjectCli = new JSONObject(json);
                return jsonObjectCli;
            }
            else {
                return null;
            }
        }catch (Exception e){
            return null;
        }
    }*/


    @Override
    protected JSONObject doInBackground(String... params){
        JSONObject jsonObject = null;

        try{
            String appName = Util.getProperty("Nombre", context);

            String locationUrl = Util.getProperty("LocationUrl", context);
            locationUrl = locationUrl.replace("\n", "");
            locationUrl = locationUrl+"/productos?from=pasarela";

            URL url = new URL("https://"+ Util.getProperty("IP_PASARELA", context)+":8443/pasarela-web/rest/compra/compraDatos");

            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier(){
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }});
            SSLContext contextSSL = SSLContext.getInstance("TLS");
            contextSSL.init(null, new X509TrustManager[]{new X509TrustManager(){
                public void checkClientTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {}
                public void checkServerTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {}
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }}}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(
                    contextSSL.getSocketFactory());

            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("accept", "application/json");

            jsonObject = new JSONObject();
            jsonObject.put("numOrden", params[0]);
            jsonObject.put("empresa", appName);
            jsonObject.put("monto", params[1]);
            jsonObject.put("url", "");

            String input = jsonObject.toString();
            OutputStream os = conn.getOutputStream();
            os.write(input.getBytes());
            os.flush();
            os.close();
            Long result = (long) conn.getResponseCode();

            if(result == 200){
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                JSONObject jsonObjectCli = new JSONObject(response.toString());
                return jsonObjectCli;
            }
            else{
                return null;
            }
        }catch (Exception e){
            return null;
        }
    }


    protected void onPostExecute(JSONObject result){

        if(result != null){
            Intent intent = new Intent("compra-redirect");
            intent.putExtra("param", result.toString());
            LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(intent);
        }
        else{

        }

    }

}
