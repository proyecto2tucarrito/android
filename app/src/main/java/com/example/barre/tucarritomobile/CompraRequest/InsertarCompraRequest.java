package com.example.barre.tucarritomobile.CompraRequest;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Base64;

import com.example.barre.tucarritomobile.CarritoApp;
import com.example.barre.tucarritomobile.Carro.Item;
import com.example.barre.tucarritomobile.Cliente.ClienteAreaActivity;
import com.example.barre.tucarritomobile.LoginActivity;
import com.example.barre.tucarritomobile.Util;
import com.facebook.Profile;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.DecimalFormat;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

/**
 * Created by Barre on 20-Nov-17.
 */
public class InsertarCompraRequest extends AsyncTask<String, Integer, JSONObject> {

    private String mail;
    private CarritoApp carritoApp;
    private Context context;
    private ProgressDialog loading;

    public InsertarCompraRequest(Context context, ProgressDialog loading) {
        this.context = context.getApplicationContext();
        this.loading = loading;
    }

    public InsertarCompraRequest(Context context) {
        this.context = context.getApplicationContext();
        //this.loading = loading;
    }

    /*@Override
    protected JSONObject doInBackground(String... params){
        JSONObject jsonObject = null;
        HttpClient httpClient = new DefaultHttpClient();

        try{
            String locationUrl = Util.getProperty("LocationUrl", context);
            byte[] locationUrlByte = locationUrl.getBytes();
            locationUrl = Base64.encodeToString(locationUrlByte, Base64.DEFAULT);
            locationUrl = locationUrl.replace("\n", "");

            HttpPost httpPost = new HttpPost("http://"+ Util.getProperty("IP", context)+":8080/tuCarrito-web/rest/compra/registro/"+locationUrl);
            httpPost.setHeader("content-type", "application/json");
            httpPost.setHeader("accept", "application/json");



            Double monto_total = 0.0;

            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
            mail = pref.getString("mail", null);

            carritoApp = (CarritoApp) context;
            monto_total = carritoApp.carro.getPrecioTotalDouble();

            jsonObject = new JSONObject();
            jsonObject.put("id", 0);
            jsonObject.put("estado", "PENDIENTE");
            jsonObject.put("monto_total", monto_total);
            JSONObject jsonObjectCliente = new JSONObject();
            jsonObjectCliente.put("mail", mail);
            jsonObject.put("cliente", jsonObjectCliente);

            JSONArray jsonArrayLineasCompra = new JSONArray();
            List<Item> listaItem = carritoApp.carro.getList();
            try {
                for (int i=0; i < listaItem.size(); i++) {
                    Item item = listaItem.get(i);

                    JSONObject jsonObjectLinea = new JSONObject();
                    jsonObjectLinea.put("id", 0);
                    jsonObjectLinea.put("cantidad", item.getCantidad());
                    jsonObjectLinea.put("comentario", null);
                    jsonObjectLinea.put("precio", item.getProducto().getDouble("precioConDcto"));
                    JSONObject jsonObjectProducto = new JSONObject(item.getProducto().toString());
                    jsonObjectLinea.put("producto", jsonObjectProducto);

                    jsonArrayLineasCompra.put(jsonObjectLinea);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            jsonObject.put("lineas_compra", jsonArrayLineasCompra);
            jsonObject.put("fecha_alta", null);
            jsonObject.put("pago", null);

            StringEntity entity = new StringEntity(jsonObject.toString());
            httpPost.setEntity(entity);

            HttpResponse httpResponse = httpClient.execute(httpPost);
            if(httpResponse.getStatusLine().getStatusCode() == 200)
            {
                String json = EntityUtils.toString(httpResponse.getEntity());
                JSONObject jsonObjectCli = new JSONObject(json);
                return jsonObjectCli;
            }
            else {
                return null;
            }
        }catch (Exception e){
            return null;
        }
    }*/

    @Override
    protected JSONObject doInBackground(String... params){
        JSONObject jsonObject = null;

        try{
            String locationUrl = Util.getProperty("LocationUrl", context);
            byte[] locationUrlByte = locationUrl.getBytes();
            locationUrl = Base64.encodeToString(locationUrlByte, Base64.DEFAULT);
            locationUrl = locationUrl.replace("\n", "");

            URL url = new URL("https://"+ Util.getProperty("IP", context)+":8443/tuCarrito-web/rest/compra/registro/"+locationUrl);

            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier(){
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }});
            SSLContext contextSSL = SSLContext.getInstance("TLS");
            contextSSL.init(null, new X509TrustManager[]{new X509TrustManager(){
                public void checkClientTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {}
                public void checkServerTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {}
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }}}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(
                    contextSSL.getSocketFactory());

            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("accept", "application/json");

            Double monto_total = 0.0;

            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
            mail = pref.getString("mail", null);

            carritoApp = (CarritoApp) context;
            monto_total = carritoApp.carro.getPrecioTotalDouble();

            jsonObject = new JSONObject();
            jsonObject.put("id", 0);
            jsonObject.put("estado", "PENDIENTE");
            jsonObject.put("monto_total", monto_total);
            JSONObject jsonObjectCliente = new JSONObject();
            jsonObjectCliente.put("mail", mail);
            jsonObject.put("cliente", jsonObjectCliente);

            JSONArray jsonArrayLineasCompra = new JSONArray();
            List<Item> listaItem = carritoApp.carro.getList();
            try {
                for (int i=0; i < listaItem.size(); i++) {
                    Item item = listaItem.get(i);

                    JSONObject jsonObjectLinea = new JSONObject();
                    jsonObjectLinea.put("id", 0);
                    jsonObjectLinea.put("cantidad", item.getCantidad());
                    jsonObjectLinea.put("comentario", null);
                    jsonObjectLinea.put("precio", item.getProducto().getDouble("precioConDcto"));
                    JSONObject jsonObjectProducto = new JSONObject(item.getProducto().toString());
                    jsonObjectLinea.put("producto", jsonObjectProducto);

                    jsonArrayLineasCompra.put(jsonObjectLinea);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            jsonObject.put("lineas_compra", jsonArrayLineasCompra);
            jsonObject.put("fecha_alta", null);
            jsonObject.put("pago", null);

            String input = jsonObject.toString();
            OutputStream os = conn.getOutputStream();
            os.write(input.getBytes());
            os.flush();
            os.close();
            Long result = (long) conn.getResponseCode();

            if(result == 200){
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                JSONObject jsonObjectCli = new JSONObject(response.toString());
                return jsonObjectCli;
            }
            else{
                return null;
            }
        }catch (Exception e){
            return null;
        }
    }


    protected void onPostExecute(JSONObject result){

        if(result != null){
            Intent intent = new Intent("compra-insertar");
            intent.putExtra("compra", result.toString());
            LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(intent);
        }
        else{

        }

    }

}
