package com.example.barre.tucarritomobile.Firebase;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.example.barre.tucarritomobile.ClienteRequest.UpdateTokenClienteRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by Daniel Alvarez on 8/25/16.
 * Copyright © 2016 Alvarez.tech. All rights reserved.
 */
public class MiFirebaseInstanceIdService extends FirebaseInstanceIdService {

    public static final String TAG = "NOTICIAS";


    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        String token = FirebaseInstanceId.getInstance().getToken();
        String id = FirebaseInstanceId.getInstance().getId();


        Log.d(TAG, "Token: " + token);

        Log.d(TAG, "Id: " + id);

        enviarTokenAlServidor(token, id);
    }

    private void enviarTokenAlServidor(String token, String id) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        String mail = pref.getString("mail", null);

        if(mail != null){
            new UpdateTokenClienteRequest(this).execute(token, id, mail);

        }

    }
}
