package com.example.barre.tucarritomobile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.barre.tucarritomobile.ClienteRequest.RegisterClienteRequest;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;

/**
 * Created by Barre on 06-Nov-17.
 */
public class LoginActivity extends AppCompatActivity implements
        GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {



    private LoginButton loginButton;
    private CallbackManager callbackManager;
    private static final int OUR_REQUEST_CODE = 49404; //Un codigo de error cualquiera
    private ProgressDialog mConnectionProgressDialog;
    private CarritoApp carritoApp;
    private static final String TAG = "SIGNING";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        RelativeLayout rLayout = (RelativeLayout) findViewById (R.id.rLogin);
        /*try {
            Bitmap bitmap = decodeBase64(Util.getProperty("ImagenFondo", getApplicationContext()));
            Drawable d = new BitmapDrawable(getResources(), bitmap);

        } catch (IOException e) {
            e.printStackTrace();
        }*/

        Drawable d = ContextCompat.getDrawable(getApplicationContext(), R.drawable.carrito_fondo);
        rLayout.setBackground(d);

        carritoApp = (CarritoApp) getApplicationContext();

        //Google login
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();


        // Build a GoogleApiClient with access to GoogleSignIn.API and the options above.
        carritoApp.mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        // Connect our sign in, sign out and disconnect buttons.
        findViewById(R.id.sign_in_button).setOnClickListener(this);

        // Configure the ProgressDialog that will be shown if there is a
        // delay in presenting the user with the next sign in step.
        mConnectionProgressDialog = new ProgressDialog(this);
        mConnectionProgressDialog.setMessage("Signing in...");
        //Fin

        callbackManager = CallbackManager.Factory.create();

        loginButton = (LoginButton) findViewById(R.id.loginButton);

        loginButton.setReadPermissions(Arrays.asList("email", "public_profile"));

        //LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email"));

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {
                                String email = "";
                                try {
                                    email = object.getString("email");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                if(email.equals("")){
                                    logoutEmailEmpty();
                                }else{
                                    saveRedSocialAndTipoServicio("Facebook");
                                    goMainScreen(email);
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "email");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Toast.makeText(getApplicationContext(), R.string.cancel_login, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(getApplicationContext(), R.string.error_login, Toast.LENGTH_SHORT).show();
            }
        });


    }

    public void saveRedSocialAndTipoServicio(String red){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = pref.edit();

        String tipo = "";
        try {
            tipo = Util.getProperty("Tipo", getApplicationContext());
        } catch (IOException e) {
            e.printStackTrace();
        }
        editor.putString("Red", red);
        editor.putString("TipoServicio", tipo);
        editor.commit();
    }

    public void logoutEmailEmpty(){
        LoginManager.getInstance().logOut();
        Toast.makeText(getApplicationContext(), R.string.error_login_email, Toast.LENGTH_SHORT).show();
    }

    public static Bitmap decodeBase64(String input)
    {
        byte[] decodedBytes = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }

    public void goMainScreen(String email){
        /*ProgressDialog progreLoading = new ProgressDialog(this);
        progreLoading.setMessage("Iniciando...");
        progreLoading.setCancelable(false);
        progreLoading.show();
        new RegisterClienteRequest(this, progreLoading).execute(email);*/
        new RegisterClienteRequest(this).execute(email);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        if (requestCode == OUR_REQUEST_CODE) {
            // Hide the progress dialog if its showing.
            mConnectionProgressDialog.dismiss();

            // Resolve the intent into a GoogleSignInResult we can process.
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(carritoApp.mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly. We can try and retrieve an
            // authentication code.
            Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Checking sign in state...");
            progressDialog.show();
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(@NonNull GoogleSignInResult googleSignInResult) {
                    progressDialog.dismiss();
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }

    protected void onStop() {
        super.onStop();
        if (carritoApp.mGoogleApiClient.isConnected()) {
            carritoApp.mGoogleApiClient.disconnect();
        }
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // When we get here in an automanager activity the error is likely not
        // resolvable - meaning Google Sign In and other Google APIs will be
        // unavailable.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sign_in_button:
                Log.v(TAG, "Tapped sign in");
                // Show the dialog as we are now signing in.
                mConnectionProgressDialog.show();

                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(carritoApp.mGoogleApiClient);
                startActivityForResult(signInIntent, OUR_REQUEST_CODE);
                break;

            default:
                // Unknown id.
                Log.v(TAG, "Unknown button press");
        }
    }

    /**
     * Helper method to trigger retrieving the server auth code if we've signed in.
     */
    private void handleSignInResult(GoogleSignInResult result ) {
        if (result.isSuccess()) {

            carritoApp.acct = result.getSignInAccount();
            // Signed in successfolly, show authenticated UI.
            //GoogleSignInAccount acct = result.getSignInAccount();
            saveRedSocialAndTipoServicio("Google");
            goMainScreen(carritoApp.acct.getEmail());
            //Similarly you can get the email and photourl using acct.getEmail() and  acct.getPhotoUrl()

            //if(acct.getPhotoUrl() != null)
            //  new LoadProfileImage(imgProfilePic).execute(acct.getPhotoUrl().toString());

        }
    }
}
