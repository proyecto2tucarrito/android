package com.example.barre.tucarritomobile.ProductoRequest;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Base64;

import com.example.barre.tucarritomobile.Util;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

/**
 * Created by Barre on 16-Nov-17.
 */
public class ObtenerCategoriasRequest  extends AsyncTask<String, Integer, JSONArray> {

    private Context context;
    public ObtenerCategoriasRequest(Context context) {
        this.context = context.getApplicationContext();
    }

    /*@Override
    protected JSONArray doInBackground(String... params){
        JSONArray result = null;
        HttpClient httpClient = new DefaultHttpClient();

        try{
            String locationUrl = Util.getProperty("LocationUrl", context);
            byte[] locationUrlByte = locationUrl.getBytes();
            locationUrl = Base64.encodeToString(locationUrlByte, Base64.DEFAULT);
            locationUrl = locationUrl.replace("\n", "");

            HttpGet httpGet = new HttpGet("http://"+ Util.getProperty("IP", context)+":8080/tuCarrito-web/rest/categoria/obtener/"+locationUrl);
            httpGet.setHeader("Content-Type", "application/json");
            httpGet.setHeader("accept", "application/json");

            HttpResponse httpResponse = httpClient.execute(httpGet);
            if(httpResponse.getStatusLine().getStatusCode() == 200)
            {
                String json = EntityUtils.toString(httpResponse.getEntity());
                JSONArray nameArray = new JSONArray(json);
                result = nameArray;
            }
        }catch (Exception e){

        }
        return result;
    }*/

    @Override
    protected JSONArray doInBackground(String... params){
        JSONArray result = null;

        try{
            String locationUrl = Util.getProperty("LocationUrl", context);
            byte[] locationUrlByte = locationUrl.getBytes();
            locationUrl = Base64.encodeToString(locationUrlByte, Base64.DEFAULT);
            locationUrl = locationUrl.replace("\n", "");

            URL url = new URL("https://"+ Util.getProperty("IP", context)+":8443/tuCarrito-web/rest/categoria/obtener/"+locationUrl);

            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier(){
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }});
            SSLContext contextSSL = SSLContext.getInstance("TLS");
            contextSSL.init(null, new X509TrustManager[]{new X509TrustManager(){
                public void checkClientTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {}
                public void checkServerTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {}
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }}}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(
                    contextSSL.getSocketFactory());

            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("accept", "application/json");

            Long resultCode = (long) conn.getResponseCode();

            if(resultCode == 200){
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                JSONArray nameArray = new JSONArray(response.toString());
                result = nameArray;
            }
        }catch (Exception e){

        }
        return result;
    }


    protected void onPostExecute(JSONArray result){
        if(result != null){
            Intent intent = new Intent("cliente-categorias");
            intent.putExtra("listaCategorias", result.toString());
            LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(intent);

        }

    }

}

